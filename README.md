# Le PIAF (Portail inter assos Fédérées)

Une appli permettant la simplification de l'organisation associatif.
## About

### Actualitées

Chaque assos à la possibilité de publier des actualitées qui seront visibles sur la page de chaque assos, mais aussi sur un feed présent sur la page d'accueil.

### Event

Cette appli permet aussi la gestion d'évènements avec un système de formulaire lors de la réservation.

### Location

Certaines assos ont des stalles qui sont mises à disposition des cotisants. Cette appli cherche à simplifier cette gestion en proposant aux gestionnaires dela salles un endroit ou centraliser toutes les demandes, les accepter/refuser et avoir un historique des entrées (qui à ouvert à qui et quand).

### Store

Présence d'un store. 

**NB:** Le payement n'est pas prit en charge.

## How does it works ?

All the application data is stored in matrix.

> The actual php backend will be removed.

All the associations, events, store items are in fact matrix rooms.

See [here](doc/backend_matrix.md) for the actual documentation.

## Building

```bash
flutter packages pub run build_runner watch
```

Building for linux

```bash
flutter build linux
```

```
sudo apt install libwebkit2gtk-4.0-dev libolm3 libsecret-1-dev libjsoncpp-dev libgtk-3-dev
```