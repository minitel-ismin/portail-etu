import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:emse_bde_app/pages/app_wrapper.dart';
import 'package:emse_bde_app/pages/assos/asso_detail_page.dart';
import 'package:emse_bde_app/pages/assos/asso_page.dart';
import 'package:emse_bde_app/pages/assos/asso_wrapper_page.dart';
import 'package:emse_bde_app/pages/assos/assos_list.dart';
import 'package:emse_bde_app/pages/bookings/bookingConfirmation.dart';
import 'package:emse_bde_app/pages/bookings/bookingPage.dart';
import 'package:emse_bde_app/pages/event/edit/event_edit_booking_page.dart';
import 'package:emse_bde_app/pages/event/edit/event_edit_description_page.dart';
import 'package:emse_bde_app/pages/event/edit/event_edit_page.dart';
import 'package:emse_bde_app/pages/event/event_booking_detail_page.dart';
import 'package:emse_bde_app/pages/event/event_bookings_page.dart';
import 'package:emse_bde_app/pages/event/event_create_page.dart';
import 'package:emse_bde_app/pages/event/event_detail_page.dart';
import 'package:emse_bde_app/pages/event/event_forms_page.dart';
import 'package:emse_bde_app/pages/event/events_list_page.dart';
import 'package:emse_bde_app/pages/feed_page.dart';
import 'package:emse_bde_app/pages/home_page.dart';
import 'package:emse_bde_app/pages/rentals/rental_create_page.dart';
import 'package:emse_bde_app/pages/rentals/rental_detail_page.dart';
import 'package:emse_bde_app/pages/rentals/rentals_page.dart';
import 'package:emse_bde_app/pages/settings/settings_portail_profile_page.dart';
import 'package:emse_bde_app/pages/settings/settings_profile_page.dart';
import 'package:emse_bde_app/pages/settings/settings_security_page.dart';
import 'package:emse_bde_app/pages/settings/settings_theme_page.dart';
import 'package:emse_bde_app/pages/store/store_item_checkout_page.dart';
import 'package:emse_bde_app/pages/store/store_item_creation.dart';
import 'package:emse_bde_app/pages/store/store_item_page.dart';
import 'package:emse_bde_app/pages/store/store_page.dart';
import 'package:emse_bde_app/pages/user/roles/roles_detail_page.dart';
import 'package:emse_bde_app/pages/user/roles/roles_list_page.dart';
import 'package:emse_bde_app/pages/user/user_account.dart';
import 'package:emse_bde_app/pages/user/user_detail_page.dart';
import 'package:emse_bde_app/pages/user/users_list_page.dart';
import 'package:emse_bde_app/pages/welcome/login/login_CAS.dart';
import 'package:emse_bde_app/pages/welcome/login/loginMatrixPage.dart';
import 'package:emse_bde_app/pages/welcome/welcome_page.dart';
import 'package:minestrix_chat/view/room_list/room_list_room.dart';
import 'package:minestrix_chat/view/room_list/room_list_space.dart';

import 'pages/chats/room_list_page.dart';
import 'pages/chats/room_list_wrapper.dart';

const chatsWrapper = AutoRoute(
    path: 'rooms',
    page: RoomListWrapper,
    name: 'RoomListWrapperRoute',
    children: [
      AutoRoute(path: '', page: RoomListPage, initial: true),
      AutoRoute(path: 'space', name: 'RoomListSpaceRoute', page: RoomListSpace),
      AutoRoute(path: ':roomId', name: 'RoomListRoomRoute', page: RoomListRoom),
    ]);

// @CupertinoAutoRouter
// @AdaptiveAutoRouter
// @CustomAutoRouter
@CustomAutoRouter(
  replaceInRouteName: 'Page,Route',
  transitionsBuilder: TransitionsBuilders.fadeIn,
  routes: <AutoRoute>[
    AutoRoute(path: '/', page: AppWrapperPage, initial: true, children: [
      AutoRoute(path: 'home', page: HomePage, initial: true),
      AutoRoute(path: 'feed', page: FeedPage),
      AutoRoute(
        path: 'events',
        page: EventsListPage,
      ),
      AutoRoute(path: 'bookings', page: EventBookingsPage),
      AutoRoute(path: 'bookings/id', page: EventBookingDetailPage),
      AutoRoute(path: 'event/create', page: EventCreatePage),
      AutoRoute(path: 'event/edit/forms', page: EventFormsPage),
      AutoRoute(path: 'event/edit', page: EventEditPage),
      AutoRoute(path: 'edit/description', page: EventEditDescriptionPage),
      AutoRoute(path: 'edit/booking', page: EventEditBookingPage),
      AutoRoute(path: 'assos', page: AssoWrapperPage, children: [
        AutoRoute(path: '', page: AssoPlaceholderPage, initial: true),
        AutoRoute(path: 'assos/details', page: AssociationPage),
        AutoRoute(path: 'assos/id', page: AssoDetailPage),
        AutoRoute(path: 'assos/roles', page: RolesListPage),
        AutoRoute(path: 'assos/roles/id', page: RolesDetailPage),
      ]),
      AutoRoute(path: 'event/detail', page: EventDetailPage),
      AutoRoute(path: 'edit/description', page: EventEditDescriptionPage),
      AutoRoute(path: 'edit/booking', page: EventEditBookingPage),
      AutoRoute(path: 'user', page: UserPage),
      AutoRoute(path: 'user/detail', page: UserDetailPage),
      AutoRoute(path: 'user/users', page: UserListPage),
      AutoRoute(path: 'user/settings/profile', page: SettingsAccountPage),
      AutoRoute(path: 'user/settings/security', page: SettingsSecurityPage),
      AutoRoute(path: 'user/settings/theme', page: SettingsThemePage),
      AutoRoute(
          path: 'user/settings/portail_profile',
          page: SettingsPortailProfilePage),
      AutoRoute(path: 'book', page: BookingPage),
      AutoRoute(path: 'book/confirmation', page: BookingConfirmationPage),
      AutoRoute(path: 'reservation', page: RentalsPage),
      AutoRoute(path: 'reservation/id', page: RentalDetailPage),
      AutoRoute(path: 'resevation/create', page: RentalCreatePage),
      AutoRoute(path: 'store', page: StorePage),
      AutoRoute(path: 'store/item/create', page: StoreItemCreatePage),
      AutoRoute(path: 'store/item/detail', page: StoreItemPage),
      AutoRoute(path: 'store/item/checkout', page: StoreItemCheckoutPage),
      chatsWrapper,
      RedirectRoute(path: '*', redirectTo: '')
    ]),
    AutoRoute(
        path: 'welcome',
        name: "WelcomeRouter",
        page: EmptyRouterPage,
        children: [
          AutoRoute(path: '', page: WelcomePage, initial: true),
          AutoRoute(path: 'login', page: LoginPage),
          AutoRoute(path: 'login_matrix', page: MatrixLoginPage),
          RedirectRoute(path: '*', redirectTo: '')
        ]),
  ],
)
class $AppRouter {}
