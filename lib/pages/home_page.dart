import 'package:auto_route/src/router/auto_router_x.dart';

import 'package:emse_bde_app/logic/matrix/extensions/event_extension.dart';
import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/pages/feed_page.dart';
import 'package:emse_bde_app/partials/asso/matrix_asso_list_item.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/event_item.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart' as matrix;

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) =>
          StreamBuilder<Object>(
              stream: m.onSync.stream,
              builder: (context, snapshot) {
                if (m.prevBatch?.isEmpty ?? true) {
                  return Center(
                      child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(width: 16),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Setting up for first use",
                              style: TextStyle(
                                  fontSize: 28, fontWeight: FontWeight.w600)),
                          SizedBox(height: 4),
                          Text("It may take a few minutes.",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w400))
                        ],
                      ),
                    ],
                  ));
                }

                return Column(
                  children: [
                    CustomHeader("Home"),
                    Expanded(
                      child: RefreshIndicator(
                        displacement: 200,
                        onRefresh: () async {
                          print("refresh");
                        },
                        child: ListView(
                          physics: const AlwaysScrollableScrollPhysics(),
                          // horizontal).
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (constraints.maxWidth > 900)
                                  Expanded(
                                      flex: 1,
                                      child: Padding(
                                          padding: const EdgeInsets.all(20.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              H2Title("Mes assos",
                                                  onPressed: () =>
                                                      context.navigateTo(
                                                          AssoWrapperRoute())),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    for (matrix.Room r in m
                                                        .getAssoRooms()
                                                      ..sort((a, b) => a
                                                                      .lastEvent !=
                                                                  null &&
                                                              b.lastEvent !=
                                                                  null
                                                          ? b.lastEvent!
                                                              .originServerTs
                                                              .compareTo(a
                                                                  .lastEvent!
                                                                  .originServerTs)
                                                          : 0))
                                                      MatrixAssoListItem(
                                                          assos: r,
                                                          onPressed: () {
                                                            AssociationData d =
                                                                AssociationData
                                                                    .fromRoom(
                                                                        r);
                                                            context.navigateTo(
                                                                AssoWrapperRoute(
                                                                    children: [
                                                                  AssociationRoute(
                                                                      assoData:
                                                                          d)
                                                                ]));
                                                          }),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ))),
                                Expanded(
                                  flex: 2,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          H2Title("Prochain évènements",
                                              onPressed: () => context
                                                  .navigateTo(EventsRoute())),
                                          SizedBox(height: 8),
                                          FutureBuilder<List<PortailEvent>>(
                                              future: getEvents(),
                                              builder: (context, snap) {
                                                if (snap.hasData == false)
                                                  return Center(
                                                      child:
                                                          CircularProgressIndicator());

                                                Iterable<PortailEvent> events = snap
                                                    .data!
                                                    .where((event) =>
                                                        event.type ==
                                                            PortailEventTypes
                                                                .event &&
                                                        event.date != null &&
                                                        event.date!
                                                                .millisecondsSinceEpoch >
                                                            DateTime.now()
                                                                .millisecondsSinceEpoch)
                                                    .take(3);
                                                if (events.isEmpty)
                                                  return Text(
                                                      "Aucun prochain évènements.");

                                                return Column(
                                                  children: [
                                                    for (PortailEvent data
                                                        in events
                                                            .toList()
                                                            .reversed)
                                                      EventItem(data),
                                                  ],
                                                );
                                              }),
                                          SizedBox(height: 24),
                                          ConstrainedBox(
                                            constraints:
                                                BoxConstraints(maxWidth: 500),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                H2Title("Actualitées",
                                                    onPressed: () =>
                                                        context.pushRoute(
                                                            FeedRoute())),
                                                SizedBox(height: 8),
                                                FeedView(
                                                  key: Key("feed_view"),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                );
              }),
    );
  }

  Future<List<PortailEvent>> getEvents() async {
    final m = Matrix.of(context).client;
    List<PortailEvent> events = await m.getAssoEvents().toList();
    return events;
  }
}
