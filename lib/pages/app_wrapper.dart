import 'package:auto_route/auto_route.dart';
import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:emse_bde_app/partials/navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

import '../partials/side_menu.dart';
import '../router.gr.dart';

class AppWrapperPage extends StatefulWidget {
  const AppWrapperPage({Key? key}) : super(key: key);

  @override
  State<AppWrapperPage> createState() => _AppWrapperPageState();
}

class _AppWrapperPageState extends State<AppWrapperPage> {
  static const displayAppBarList = {
    "/home",
    "/events",
    "/assos",
    "/user",
    "/rooms"
  };
  bool displayAppBar = false;

  Future<bool>? joinRooms;
  Future<bool> autoJoin(Client m) async {
    if (m.prevBatch == null) await m.onSync.stream.first;
    await m.joinAssoAndPortailChilds();
    return true;
  }

  /// init the client
  Future<bool> init(BuildContext context) async {
    final m = Matrix.of(context).client;
    // await for the matrix client to load
    await m.accountDataLoading;
    await m.userDeviceKeysLoading;
    await m.roomsLoading;

    if (joinRooms == null) {
      joinRooms = autoJoin(m);
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    // Setup the context for the key verification contexts
    Matrix.of(context).navigatorContext = context;

    return FutureBuilder(
        future: init(context),
        builder: (context, snap) {
          return LayoutBuilder(builder: (context, constraints) {
            bool isWideScreen = constraints.maxWidth > 900;
            return FutureBuilder(
                future: joinRooms,
                builder: (context, snap) {
                  return Scaffold(
                      drawer: SafeArea(
                          child: Drawer(
                        child: SideMenu(),
                      )),
                      body: Column(
                        children: [
                          if (isWideScreen) NavBarDesktop(),
                          Expanded(child: AutoRouter(
                            builder: (context, widget) {
                              final shouldDisplayAppBar = displayAppBarList
                                  .contains(AutoRouterDelegate.of(context)
                                      .urlState
                                      .uri
                                      .toString());
                              if (displayAppBar != shouldDisplayAppBar) {
                                SchedulerBinding.instance
                                    .addPostFrameCallback((_) {
                                  setState(() {
                                    displayAppBar = shouldDisplayAppBar;
                                  });
                                });
                              }
                              return widget;
                            },
                          )),
                        ],
                      ),
                      bottomNavigationBar: isWideScreen || !displayAppBar
                          ? null
                          : BottomNavigationBar(
                              backgroundColor: Theme.of(context)
                                  .scaffoldBackgroundColor
                                  .withAlpha(230),
                              currentIndex: 0,
                              onTap: (pos) {
                                print(context.routeData.name);

                                switch (pos) {
                                  case 0:
                                    context.navigateTo(HomeRoute());
                                    break;
                                  case 1:
                                    context.navigateTo(EventsRoute());
                                    break;
                                  case 2:
                                    context.navigateTo(AssoWrapperRoute());
                                    break;
                                  case 3:
                                    context.navigateTo(UserRoute());
                                    break;
                                  case 4:
                                    context.navigateTo(RoomListWrapperRoute());
                                    break;
                                  default:
                                }
                              },
                              type: BottomNavigationBarType.fixed,
                              items: [
                                  BottomNavigationBarItem(
                                    icon: Icon(Icons.home),
                                    label: 'Home',
                                  ),
                                  BottomNavigationBarItem(
                                    icon: Icon(Icons.event),
                                    label: 'Events',
                                  ),
                                  BottomNavigationBarItem(
                                    icon: Icon(Icons.people),
                                    label: 'Assos',
                                  ),
                                  BottomNavigationBarItem(
                                      icon: Icon(Icons.person), label: 'User'),
                                  BottomNavigationBarItem(
                                      icon: Icon(Icons.chat), label: 'Chat')
                                ]));
                });
          });
        });
  }
}
