import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/partials/poll/poll_widget.dart';

class EventFormsPage extends StatefulWidget {
  final PortailEvent event;
  const EventFormsPage({Key? key, required this.event}) : super(key: key);

  @override
  _EventFormsPageState createState() => _EventFormsPageState();
}

class _EventFormsPageState extends State<EventFormsPage> {
  bool fetching = false;
  Future<bool> requestHistory(Timeline t) async {
    while (t.canRequestHistory) {
      await t.requestHistory();
    }
    return true;
  }

  Future<bool>? requestHistoryInstance;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        FutureBuilder<Timeline>(
            future: widget.event.r.getTimeline(),
            builder: (context, snapT) {
              return StreamBuilder<Object>(
                  stream: widget.event.r.client.onSync.stream,
                  builder: (context, snapshot) {
                    if (snapT.hasData && requestHistoryInstance == null) {
                      requestHistoryInstance = requestHistory(snapT
                          .data!); // create only one instance which will be responsible to fetch the timeline history
                    }
                    return Column(
                      children: [
                        CustomHeader("Formulaires"),
                        snapT.hasData
                            ? FutureBuilder(
                                future: requestHistoryInstance,
                                builder: (context, snapHistory) {
                                  return Column(
                                    children: [
                                      for (Event e in snapT.data!.events.where(
                                          (event) =>
                                              event.type ==
                                                  MatrixEventTypes.pollStart &&
                                              event.redacted == false))
                                        PollWidget(
                                            event: e, timeline: snapT.data!),
                                      if (snapHistory.hasData == false)
                                        CircularProgressIndicator()
                                    ],
                                  );
                                })
                            : CircularProgressIndicator(),
                      ],
                    );
                  });
            }),
      ],
    );
  }
}
