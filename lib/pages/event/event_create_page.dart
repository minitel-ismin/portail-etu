import 'package:another_flushbar/flushbar.dart';
import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_description.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/event_edit_form.dart';
import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';

class EventCreatePage extends StatefulWidget {
  const EventCreatePage({Key? key, required this.event}) : super(key: key);
  final BasePortailEvent event;

  @override
  _EventCreatePageState createState() => _EventCreatePageState();
}

class _EventCreatePageState extends State<EventCreatePage>
    with SingleTickerProviderStateMixin {
  late BasePortailEvent _event;
  late PortailEventBooking booking;
  late PortailEventDescription description;

  String? errorText = null;

  int _selectedIndex = 0;
  final int len = 1;
  late TabController _controller;

  bool bookingEdited = false;
  bool descriptionEdited = false;

  @override
  void initState() {
    super.initState();

    _event = widget.event;
    booking = _event.eventBooking;
    description = _event.eventDescription;

    _controller = TabController(length: len, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        CustomHeader(
          "Modifier l'évènement " + (_event.name),
          actionButton: [
            Tooltip(
                message: "Preview item",
                child: IconButton(
                    icon: Icon(Icons.preview),
                    onPressed: () async {
                      await _previewEvent();
                    })),
            Tooltip(
                message: "Edit event",
                child: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () async {
                      await createEvent();
                    })),
          ],
        ),
        Expanded(
          child: TabBarView(
            children: [
              EventEditForm(
                booking: booking,
                onChanged: (e) async {
                  booking = e;
                  bookingEdited = true;
                },
              ),
            ],
            controller: _controller,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              previousButton(),
              DotStepper(
                dotCount: len,
                dotRadius: 6,
                activeStep: _selectedIndex,
                shape: Shape.circle,
                spacing: 10,
                indicator: Indicator.slide,
                onDotTapped: (tappedDotIndex) {
                  setState(() {
                    _controller.animateTo(tappedDotIndex);
                  });
                },
              ),
              nextButton()
            ],
          ),
        )
      ],
    ));
  }

  /// Returns the next button widget.
  Widget nextButton() {
    return CustomElevatedButton(
      message: _selectedIndex < len - 1 ? 'Next' : "Save",
      icon: Icons.navigate_next,
      onFuturePressed: () async {
        if (_selectedIndex < len - 1) {
          _controller.animateTo(_selectedIndex += 1);
        } else {
          await createEvent();
        }
      },
    );
  }

  /// Returns the previous button widget.
  Widget previousButton() {
    return CustomElevatedButton(
      message: "Prev",
      icon: Icons.navigate_before,
      onPressed: () {
        if (_selectedIndex > 0) {
          _controller.animateTo(_selectedIndex -= 1);
        }
      },
    );
  }

  int pos = 0;

  Future<void> _previewEvent() async {
    String? eText = booking
        .getErrorMessage(); // try to see if the registration is valid or not

    if (eText != null) {
      await Flushbar(
        title: 'Le formulaire comporte une erreur',
        message: eText,
        duration: Duration(seconds: 6),
        icon: Icon(Icons.error, color: Colors.white),
        backgroundColor: Colors.red,
        leftBarIndicatorColor: Colors.red[800],
      ).show(context);
      setState(() async {
        errorText = eText;
      }); // update the error message
    }
    /* else Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EventDetailPage(_event!, isPreview: true)));*/ // TODO: see if we can restore it
  }

// input validation

  Future<void> createEvent() async {
    String? eText = booking
        .getErrorMessage(); // try to see if the registration is valid or not

    if (eText != null) {
      await Flushbar(
        title: 'Le formulaire comporte une erreur',
        message: eText,
        duration: Duration(seconds: 6),
        icon: Icon(Icons.error, color: Colors.white),
        backgroundColor: Colors.red,
        leftBarIndicatorColor: Colors.red[800],
      ).show(context);
      setState(() async {
        errorText = eText;
      }); // update the error message
    } else {
      if (descriptionEdited) {
        await _event.setEvenDescription(description);
      }
      if (bookingEdited) {
        await _event.setEventBooking(booking);
      }

      // and navigate back
      if (Navigator.canPop(context)) context.router.pop();
    }
  }
}
