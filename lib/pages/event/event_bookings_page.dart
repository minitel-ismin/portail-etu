import 'package:auto_route/auto_route.dart';
import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

/// Display the bookings of an event
class EventBookingsPage extends StatefulWidget {
  const EventBookingsPage(this.bookings, {Key? key}) : super(key: key);

  final List<Booking> bookings;

  @override
  _EventBookingsPageState createState() => _EventBookingsPageState();
}

class _EventBookingsPageState extends State<EventBookingsPage> {
  List<Booking>? _bookings;
  @override
  Widget build(BuildContext context) {
    if (_bookings == null) _bookings = widget.bookings;

    return Scaffold(
      body: Column(
        children: [
          CustomHeader("Réservations"),
          _bookings!.length == 0
              ? Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.new_releases_outlined, size: 40),
                      SizedBox(width: 15),
                      Text("Il n'y a pas encore de réservations",
                          style: TextStyle(fontSize: 35))
                    ],
                  ),
                )
              : Flexible(
                  child: ListView(
                    children: [
                      for (Booking res in _bookings!)
                        buildFomInput(context, res),
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  buildFomInput(BuildContext context, Booking res) {
    return ListTile(
      leading: Icon(res.isChecked == true
          ? Icons.radio_button_checked
          : Icons.radio_button_unchecked),
      trailing: Icon(Icons.arrow_forward),
      title: Text(res.user.displayName ?? res.user.id),
      subtitle: Text(timeago.format(res.createdAt)),
      onTap: () async {
        // we don't need to make an http request
        await context.pushRoute(EventBookingDetailRoute(booking: res));
      },
    );
  }
}
