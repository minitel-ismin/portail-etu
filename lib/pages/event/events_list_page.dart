import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/matrix/extensions/event_extension.dart';

import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/controls/custom_list.dart';
import 'package:emse_bde_app/partials/events/event_item.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class EventsListPage extends StatefulWidget {
  const EventsListPage({Key? key}) : super(key: key);

  @override
  _EventsListPageState createState() => _EventsListPageState();
}

class _EventsListPageState extends State<EventsListPage> {
  bool refreshing = false;

  List<PortailEvent>? _events;
  List<PortailEvent>? get events => _events == null
      ? []
      : _events?.where((e) => e.type == PortailEventTypes.event).toList();

  Future<List<PortailEvent>> _load({bool refresh: false}) async {
    if (_events != null && !refresh) return _events!;

    final m = Matrix.of(context).client;
    List<PortailEvent> events = [];

    await m.roomsLoading;

    if (refresh) {
      await m
          .joinAssoAndPortailChilds(); // auto join matrix room space childrens
    }

    events.addAll(m.getAssoEvents());

    _events = events;

    return events;
  }

  Future<List<PortailEvent>> _loadAndUpdateState({bool refresh: false}) async {
    if (mounted)
      setState(() {
        refreshing = true;
      });

    await _load(refresh: refresh);

    if (mounted)
      setState(() {
        refreshing = false;
      });
    return _events!;
  }

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return StreamBuilder<Object>(
        stream: m.onSync.stream,
        builder: (context, snapshot) {
          return FutureBuilder(
              future: _load(),
              builder: (context, snap) {
                final futureEvents = events!
                    .where((e) => (e.date?.compareTo(DateTime.now()) ?? 0) >= 0)
                    .toList();

                final pastEvents = events!
                    .where((e) => (e.date?.compareTo(DateTime.now()) ?? 0) < 0)
                    .toList();

                return RefreshIndicator(
                    onRefresh: () async {
                      await _loadAndUpdateState(refresh: true);
                    },
                    child: CustomList(
                        title: "Events",
                        hasData: snap.hasData && !refreshing,
                        actions: [
                          IconButton(
                              icon: !refreshing
                                  ? Icon(Icons.refresh)
                                  : CircularProgressIndicator(),
                              onPressed: () async {
                                if (!refreshing)
                                  await _loadAndUpdateState(refresh: true);
                              }),
                          IconButton(
                              icon: Icon(Icons.add),
                              onPressed: () async {
                                PortailEvent? e =
                                    await PortailEvent.createEvent(m,
                                        name: "My new event", topic: "");

                                if (e != null) {
                                  await context.router
                                      .push(EventDetailRoute(event: e));
                                  await _loadAndUpdateState(refresh: true);
                                }
                              })
                        ],
                        children: [
                          if (futureEvents.isNotEmpty)
                            Row(
                              children: [
                                H2Title("Évènements à venir"),
                              ],
                            ),
                          if (futureEvents.isNotEmpty)
                            GridView.builder(
                                gridDelegate:
                                    const SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 420,
                                        mainAxisExtent: 340),
                                itemCount: futureEvents.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, pos) {
                                  final item = futureEvents[pos];
                                  return Padding(
                                    padding: const EdgeInsets.all(2),
                                    child: EventItem(item, onResult: () async {
                                      await _loadAndUpdateState(refresh: true);
                                    }),
                                  );
                                }),
                          if (pastEvents.isNotEmpty)
                            Row(
                              children: [
                                H2Title("Évènements passés"),
                              ],
                            ),
                          if (pastEvents.isNotEmpty)
                            GridView.builder(
                                gridDelegate:
                                    const SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 420,
                                        mainAxisExtent: 340),
                                itemCount: pastEvents.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, pos) {
                                  final item = pastEvents[pos];
                                  return Padding(
                                    padding: const EdgeInsets.all(2),
                                    child: EventItem(item, onResult: () async {
                                      await _loadAndUpdateState(refresh: true);
                                    }),
                                  );
                                }),
                        ]));
              });
        });
  }
}
