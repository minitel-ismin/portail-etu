import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/event_edit_title.dart';
import 'package:flutter/material.dart';

class EventEditDescriptionPage extends StatelessWidget {
  final PortailEvent event;
  EventEditDescriptionPage({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomHeader("Modifier l'évènement"),
        Expanded(
          child: EventEditTitle(
              event: event,
              onSave: () {
                Navigator.of(context).pop();
              }),
        ),
      ],
    );
  }
}
