import 'package:auto_route/auto_route.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/custom_edit/custom_edit_title.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';

class EventEditPage extends StatelessWidget {
  final PortailEvent event;
  const EventEditPage({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CustomHeader(
          "Modifier l'évènement " + (event.name),
        ),
        CustomEditTitle(event: event),
        if (event.canSetEventDescription)
          ListTile(
              title: Text("Général"),
              leading: Icon(Icons.event),
              subtitle:
                  Text("Modifier les informations générales de l'évènement"),
              trailing: Icon(Icons.navigate_next),
              onTap: () {
                context.pushRoute(EventEditDescriptionRoute(event: event));
              }),
        if (event.canSetEventBooking)
          ListTile(
              title: Text("Réservation"),
              leading: Icon(Icons.book_online),
              subtitle: Text(
                  "Modifier les informations de réservation de l'évènement"),
              trailing: Icon(Icons.navigate_next),
              onTap: () {
                context.pushRoute(EventEditBookingRoute(event: event));
              }),
        H2Title("Danger zone"),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  "Si le sondage de présence à été supprimé, vous pouvez le recréer avec ce bouton."),
              Text(
                  "Attention, ceci remplacera toutes les réponses précedentes.",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 8),
              CustomElevatedButton(
                  message: "Recréer le sondage",
                  icon: Icons.poll,
                  color: Colors.red,
                  onFuturePressed: () async {
                    await event.setPollAttendance();
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
