import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:emse_bde_app/partials/portail_typo.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:flutter/material.dart';

/// Display a specifi booking for an event
class EventBookingDetailPage extends StatelessWidget {
  final Booking booking;

  const EventBookingDetailPage(this.booking, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CustomHeader("Réservation de " + (booking.user.displayName ?? 'null')),
        H1Title("Reservation"),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              for (BookingResponse res in booking.response.responses.values)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(res.id.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 24)),
                      Column(
                        children: [
                          for (String o in res.answers)
                            Row(
                              children: [
                                Text(o),
// TODO: add back price
                                /*
                                    Text("🛒 " + o),
                                    if (o.price != 0)
                                      Text(
                                          " ( " + o.price.toString() + " 💶 )"),*/
                              ],
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
              SizedBox(height: 20),
              Card(
                  child: Padding(
                padding: const EdgeInsets.all(14),
                child: Text("Coût : " + booking.cost.toString() + " euros",
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
              ))
            ],
          ),
        ),
        if (booking.response.e?.canRedact == true)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                PortailH2("Danger zone"),
                CustomElevatedButton(
                    icon: Icons.delete,
                    message: "Delete",
                    color: Colors.red,
                    onPressed: () async {
                      await booking.response.e!.room
                          .redactEvent(booking.response.e!.eventId);

                      await context.router.pop<String>("delete");
                    }),
              ],
            ),
          )
      ],
    );
  }
}
