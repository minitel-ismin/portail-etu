import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/partials/controls/custom_list.dart';
import 'package:flutter/material.dart';

class BookingConfirmationPage extends StatelessWidget {
  const BookingConfirmationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomList(title: 'Comfimation de réservation', children: [
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text("Votre réservation a bien été prise en compte !",
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.w600)),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: ElevatedButton(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("Ok", style: TextStyle(fontSize: 20)),
                  ),
                  style: ElevatedButton.styleFrom(primary: Colors.green),
                  onPressed: () async {
                    if (Navigator.canPop(context)) await context.router.pop();
                  }),
            ),
          ],
        ),
      ),
    ]);
  }
}
