import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_question.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/form_input_partial.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/helpers/preview_indicator.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';

class BookingPage extends StatefulWidget {
  const BookingPage(this.e, {this.reservationId, this.isPreview: false});

  final PortailEvent? e;
  final int? reservationId;
  final bool isPreview;

  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  PortailEvent? event;
  bool sendingReservation = false;

  @override
  void initState() {
    super.initState();
    event = widget.e;
  }

  Map<String, BookingResponse> responses = {};

  @override
  Widget build(BuildContext context) {
    String titleName = "Reservation " + event!.name;
    if (event!.type == PortailEventTypes.store_item)
      titleName = "Acheter " + event!.name;

    return Scaffold(
        body: RefreshIndicator(
      onRefresh: () async {},
      child: ListView(
        children: [
          CustomHeader(titleName),
          if (widget.isPreview) PreviewIndicator(),
          if (event!.type == PortailEventTypes.event)
            H1Title("Gardez moi une place pour l'event : " + event!.name),
          if (event!.type == PortailEventTypes.store_item)
            H1Title("Acheter " + event!.name),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (event!.eventBooking.shotgunListLength != 0)
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Icon(Icons.timelapse),
                              SizedBox(width: 12),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Évènement au shotgun",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600)),
                                  if (event!.bookingResponses.length > 0)
                                    Text("Déjà " +
                                        event!.bookingResponses.length
                                            .toString() +
                                        " places parties !!"),
                                  if (event!.remainingPlaces != 0)
                                    Text(event!.remainingPlaces.toString() +
                                        " places restantes")
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    for (BookingQuestion input
                        in event!.eventBooking.form_questions)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FormInputPartial(
                            input,
                            responses[input.id] ?? BookingResponse()
                              ..id = input.id, onRefresh: () {
                          setState(() {});
                        }),
                      )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(14),
                child: Card(
                    child: Padding(
                  padding: const EdgeInsets.all(14),
                  child: Text("Coût : " + event!.eventBooking.displayCost,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
                )),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: !widget.isPreview ? Colors.green : Colors.red),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          sendingReservation
                              ? SizedBox(
                                  height: 18,
                                  width: 18,
                                  child: CircularProgressIndicator(
                                      color: Colors.white))
                              : Icon(Icons.send),
                          SizedBox(width: 10),
                          Text("Reserver"),
                        ],
                      ),
                    ),
                    onPressed: () async {
                      if (!sendingReservation && !widget.isPreview)
                        await reserver(event!.eventBooking.form_questions);
                    }),
              )
            ],
          )
        ],
      ),
    ));
  }

  Future<void> reserver(List<BookingQuestion> inputs) async {
    setState(() {
      sendingReservation = true;
    });

    await widget.e?.book(responses);
    if (mounted)
      setState(() {
        sendingReservation = false;
      });
    context.router.pop();
    context.pushRoute(BookingConfirmationRoute());
  }
}
