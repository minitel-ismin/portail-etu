import 'package:another_flushbar/flushbar.dart';
import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/rental_item.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/custom_edit/custom_edit_title.dart';
import 'package:flutter/material.dart';

class RentalCreatePage extends StatefulWidget {
  const RentalCreatePage({Key? key, required this.rental}) : super(key: key);

  final RentalItem rental;

  @override
  _RentalCreatePageState createState() => _RentalCreatePageState();
}

class _RentalCreatePageState extends State<RentalCreatePage> {
  late RentalItem rental;

  @override
  void initState() {
    super.initState();
    rental = widget.rental;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          CustomHeader("Modifier item à réserver"),
          Column(
            children: [
              CustomEditTitle(event: rental),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.edit),
                          SizedBox(width: 12),
                          Text("Modifier la salle"),
                        ],
                      ),
                    ),
                    onPressed: () async {
                      if (await editEvent()) {
                        // and navigate back
                        if (Navigator.canPop(context)) context.router.pop();
                      }
                    }),
              )
            ],
          ),
        ],
      ),
    );
  }

  String? getErrorMessage() {
    if (rental.name == "") return "Le nom de la salle ne peut pas être nul";
    return null;
  }

  Future<bool> editEvent() async {
    String? eText =
        getErrorMessage(); // try to see if the registration is valid or not

    if (eText != null) {
      await Flushbar(
        title: 'Le formulaire comporte une erreur',
        message: eText,
        duration: Duration(seconds: 6),
        icon: Icon(Icons.error, color: Colors.white),
        backgroundColor: Colors.red,
        leftBarIndicatorColor: Colors.red[800],
      ).show(context);
    } else {
      var booking = rental.eventBooking;

      booking.isBookable = true;
      await rental.setEventBooking(booking);

      return true;
    }
    return false;
  }
}
