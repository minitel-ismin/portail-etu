import 'package:collection/collection.dart';

import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/json/user/portailUser.dart';
import 'package:emse_bde_app/partials/controls/custom_text_dialog.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:matrix/matrix.dart';

class SettingsPortailProfilePage extends StatefulWidget {
  final Room room;
  const SettingsPortailProfilePage({Key? key, required this.room})
      : super(key: key);

  @override
  _SettingsPortailProfilePageState createState() =>
      _SettingsPortailProfilePageState();
}

class _SettingsPortailProfilePageState
    extends State<SettingsPortailProfilePage> {
  bool isProfileInitialized = false;
  bool _loading = false;
  late PortailUser p;

  Future<void> _setProfile() async {
    setState(() {
      _loading = true;
    });
    await widget.room.setPortailProfile(p);
    setState(() {
      _loading = false;
    });
  }

  void initProfile() {
    final m = Matrix.of(context).client;

    List<PortailUser> users = widget.room.getPortailUsers();
    PortailUser? profile = users.firstWhereOrNull((u) => u.userID == m.userID);

    if (profile == null) {
      profile = PortailUser(m.userID!);
    }

    p = profile;
    isProfileInitialized = true;
  }

  @override
  Widget build(BuildContext context) {
    if (!isProfileInitialized) initProfile();

    return ListView(
      children: [
        CustomHeader("Mon profile",
            actionButton: [if (_loading) CircularProgressIndicator()]),
        Row(
          children: [
            H2Title("Portail " + widget.room.name),
          ],
        ),
        ListTile(
            title: Text("A propos"),
            leading: Icon(Icons.title),
            subtitle: MarkdownBody(data: p.about ?? "Set me"),
            trailing: Icon(Icons.edit),
            onTap: () async {
              String? newName = await showDialog(
                context: context,
                builder: (context) => CustomTextDialog(
                    initialText: p.about ?? "",
                    name: "A propos",
                    maxLines: null),
              );
              if (newName != null) {
                p.about = newName;
                await _setProfile();
              }
            }),
        ListTile(
            title: Text("Promo"),
            leading: Icon(Icons.list_alt),
            subtitle: Text(p.promo ?? "EI19, EI20, EI21... ?"),
            trailing: Icon(Icons.edit),
            onTap: () async {
              String? newName = await showDialog(
                  context: context,
                  builder: (context) => CustomTextDialog(
                      initialText: p.promo ?? "", name: "Ma promo"));
              if (newName != null) {
                p.promo = newName;
                await _setProfile();
              }
            }),
        ListTile(
            title: Text("Mon anniv"),
            leading: Icon(Icons.bedroom_baby),
            subtitle: Text(p.birthday != null
                ? ("Le " +
                    p.birthday!.day.toString() +
                    "/" +
                    p.birthday!.month.toString() +
                    "/" +
                    p.birthday!.year.toString())
                : "EI19, EI20, EI21... ?"),
            trailing: Icon(Icons.edit),
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (context) => Dialog(
                        child: CalendarDatePicker(
                            initialDate: p.birthday ?? DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime.now().add(Duration(days: 365)),
                            onDateChanged: (newDate) async {
                              p.birthday = newDate;
                              await _setProfile();
                              Navigator.of(context).pop();
                            }),
                      ));
            }),
      ],
    );
  }
}
