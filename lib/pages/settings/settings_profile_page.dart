import 'package:adaptive_dialog/adaptive_dialog.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class SettingsAccountPage extends StatefulWidget {
  const SettingsAccountPage({Key? key}) : super(key: key);

  @override
  _SettingsAccountPageState createState() => _SettingsAccountPageState();
}

class _SettingsAccountPageState extends State<SettingsAccountPage> {
  TextEditingController? displayNameController;
  bool savingDisplayName = false;

  @override
  Widget build(BuildContext context) {
    final sclient = Matrix.of(context).client;

    return ListView(
      children: [
        CustomHeader("Account"),
        ListTile(
            title: Text("Your user ID:"),
            subtitle: Text(sclient.userID ?? "null")),
        FutureBuilder(
            future: sclient.getUserProfile(sclient.userID!),
            builder: (context, AsyncSnapshot<ProfileInformation> p) {
              if (displayNameController == null && p.hasData == true) {
                displayNameController = new TextEditingController(
                    text: (p.data?.displayname ?? sclient.userID!));
              }

              return ListTile(
                  leading: savingDisplayName
                      ? CircularProgressIndicator()
                      : MatrixImageAvatar(
                          client: sclient,
                          url: p.data?.avatarUrl,
                          width: 48,
                          height: 48,
                          shape: MatrixImageAvatarShape.rounded,
                          defaultIcon: Icon(Icons.person, size: 32),
                        ),
                  title: Text("Edit display name"),
                  trailing: Icon(Icons.edit),
                  subtitle: Text(p.data?.displayname ?? sclient.userID!),
                  onTap: () async {
                    List<String>? results = await showTextInputDialog(
                      context: context,
                      textFields: [
                        DialogTextField(
                            hintText: "Your display name",
                            initialText: p.data?.displayname ?? "")
                      ],
                      title: "Set display name",
                    );
                    if (results?.isNotEmpty == true) {
                      setState(() {
                        savingDisplayName = true;
                      });
                      await sclient.setDisplayName(
                          sclient.userID!, results![0]);
                      setState(() {
                        savingDisplayName = false;
                      });
                    }
                  });
            }),
        ListTile(
            iconColor: Colors.red,
            title: Text("Logout"),
            trailing: Icon(Icons.logout),
            onTap: () async {
              await sclient.logout();
              if (Navigator.of(context).canPop()) Navigator.of(context).pop();
            }),
      ],
    );
  }
}
