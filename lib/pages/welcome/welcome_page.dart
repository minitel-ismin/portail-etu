import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:minestrix_chat/partials/login/login_card.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/plugin_web_stub.dart'
    if (dart.library.html) 'package:emse_bde_app/logic/pluginWebLogin.dart';
import 'package:emse_bde_app/pages/welcome/login/login_CAS.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  bool matrixLogin = false;
  @override
  Widget build(BuildContext context) {
    final client = Matrix.of(context).client;
    return Scaffold(body: Center(
      child: LayoutBuilder(builder: (context, layout) {
        if (layout.maxWidth < 1000)
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 60.0),
                  child: WelcomeTitle(),
                ),
                WelcomeButtons(),
              ],
            ),
          );

        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage('assets/images/piaf.jpg'),
                          width: 200,
                          height: 200,
                        ),
                        SizedBox(height: 20),
                        Text(
                          "Le Portail Inter Assos Fédérées",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1
                              ?.copyWith(fontSize: 18),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            child: Text("Code"),
                            onPressed: () async {
                              String url =
                                  "https://gitlab.com/minitel-ismin/portail-associations/portail-etu";
                              if (await canLaunch(url)) {
                                await launch(url);
                              }
                            }),
                        TextButton(
                            child: Text("Doc"),
                            onPressed: () async {
                              await launch(
                                  "https://gitlab.com/minitel-ismin/portail-associations/portail-etu/-/blob/master/doc");
                            })
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Stack(
                children: [
                  Image(
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      image: AssetImage("assets/images/login_background.jpg")),
                  Center(
                      child: Card(
                    child: ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: 600),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: !matrixLogin
                              ? WelcomeButtons(
                                  onLoginMatrixPressed: () {
                                    setState(() {
                                      matrixLogin = true;
                                    });
                                  },
                                )
                              : Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        IconButton(
                                            onPressed: () {
                                              setState(() {
                                                matrixLogin = false;
                                              });
                                            },
                                            icon: Icon(Icons.navigate_before)),
                                        Expanded(
                                            child: Text("Login with Matrix"))
                                      ],
                                    ),
                                    Divider(),
                                    LoginMatrixCard(
                                      client: client,
                                      defaultServer: "https://matrix.emse.fr",
                                    ),
                                  ],
                                ),
                        )),
                  ))
                ],
              ),
            )
          ],
        );
      }),
    ));
  }
}

class WelcomeTitle extends StatelessWidget {
  const WelcomeTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text("Le PIAF",
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600)),
        Text("Le Portail Inter Assos Fédérées",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w400)),
      ],
    );
  }
}

class WelcomeButtons extends StatelessWidget {
  const WelcomeButtons({Key? key, this.onLoginMatrixPressed}) : super(key: key);

  final VoidCallback? onLoginMatrixPressed;

  final String matrixLoginUrl =
      "https://matrix.emse.fr/_matrix/client/r0/login/sso/redirect/saml?redirectUrl=https://myismin.emse.fr";

  _launchURLInBrowser(String url) async {
    if (await canLaunch(url)) {
      WebLogin.launchSame(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget welcomeButton(String text, Function()? onPressed) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ElevatedButton(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 15),
            child: Text(text, style: TextStyle(fontSize: 25)),
          ),
          onPressed: onPressed),
    );
  }

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return StreamBuilder<LoginState>(
        stream: Matrix.of(context).client.onLoginStateChanged.stream,
        builder: (context, snapshot) {
          final logged = m.isLogged();
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 34.0, horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(18),
                  child: Text("Connexion",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                ),
                if (logged == true)
                  ElevatedButton(
                      child: Text("Logout"),
                      onPressed: () async {
                        await m.logout();
                      }),
                if (logged == false)
                  Column(
                    children: [
                      if (false) // TODO: see if we re enable login via the school portal
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: ListTile(
                            title: Text("EMSE login",
                                style: TextStyle(fontWeight: FontWeight.w500)),
                            subtitle: Text(
                                "Connectez vous à travers le système d'authentification centralisée de l'école."),
                            onTap: () async {
                              if (kIsWeb) {
                                await _launchURLInBrowser(matrixLoginUrl);
                              } else {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => LoginPage()));
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            leading: Card(
                                child: SizedBox(
                                  height: 40,
                                  width: 40,
                                  child: Center(
                                    child: Icon(Icons.school,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onPrimary),
                                  ),
                                ),
                                color: Theme.of(context).primaryColor),
                            trailing: Icon(Icons.navigate_next),
                          ),
                        ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: ListTile(
                          title: Text("Matrix login",
                              style: TextStyle(fontWeight: FontWeight.w500)),
                          subtitle: Text(
                              "Connectez vous grâce à votre compte matrix."),
                          onTap: () async {
                            if (onLoginMatrixPressed != null) {
                              onLoginMatrixPressed?.call();
                            } else {
                              await context.pushRoute(MatrixLoginRoute());
                            }
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          leading: Card(
                            color: Theme.of(context).primaryColor,
                            child: SizedBox(
                              height: 40,
                              width: 40,
                              child: Center(
                                child: Text("[m]",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onPrimary)),
                              ),
                            ),
                          ),
                          trailing: Icon(
                            Icons.navigate_next,
                          ),
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          );
        });
  }
}
