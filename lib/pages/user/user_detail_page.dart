import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/json/user/portailUser.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/custom_view/custom_stateful_view.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/view/room_page.dart';

class UserDetailPage extends StatelessWidget {
  final PortailUser user;
  const UserDetailPage(this.user, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mc = Matrix.of(context).client;

    return Scaffold(
      body: FutureBuilder<ProfileInformation>(
          future: mc.getUserProfile(user.userID),
          builder: (context, snap) {
            return CustomStatefulView(
                header: CustomHeader("Utilisateur"),
                children: [
                  Center(
                    child: Column(
                      children: [
                        Column(
                          children: [
                            MatrixImageAvatar(
                                client: mc,
                                url: snap.data?.avatarUrl,
                                backgroundColor: Colors.blue,
                                height: 150,
                                width: 150,
                                defaultText: snap.data?.displayname,
                                defaultIcon: Icon(Icons.person, size: 80)),
                            SizedBox(
                              height: 10,
                            ),
                            H1Title(snap.data?.displayname ?? user.userID),
                          ],
                        )
                      ],
                    ),
                  ),
                  Center(
                    child: CustomElevatedButton(
                        message: "Envoyer un message",
                        icon: Icons.send,
                        onFuturePressed: () async {
                          String roomID = await mc.startDirectChat(user.userID);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Scaffold(
                                      appBar: AppBar(
                                          title: Text(snap.data?.displayname ??
                                              user.userID)),
                                      body: RoomPage(
                                          client: Matrix.of(context).client,
                                          roomId: roomID))));
                        }),
                  ),
                  SizedBox(height: 10),
                  if (user.about != null)
                    ListTile(
                      title: Text("A propos"),
                      leading: Icon(Icons.title),
                      subtitle: MarkdownBody(data: user.about!),
                    ),
                  if (user.promo != null)
                    ListTile(
                      title: Text("Promo"),
                      leading: Icon(Icons.list_alt),
                      subtitle: Text(user.promo!),
                    ),
                  if (user.birthday != null)
                    ListTile(
                        title: Text("Birthday"),
                        leading: Icon(Icons.bedroom_baby),
                        subtitle: Text("Le " +
                            user.birthday!.day.toString() +
                            "/" +
                            user.birthday!.month.toString() +
                            "/" +
                            user.birthday!.year.toString()))
                ]);
          }),
    );
  }
}
