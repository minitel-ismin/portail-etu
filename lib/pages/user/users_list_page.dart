import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/json/user/portailUser.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:emse_bde_app/partials/user/user_list_item.dart';
import 'package:flutter/material.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    List<PortailUser> users = m.getPortailUsers();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomHeader("Utilisateurs"),
        users.isEmpty
            ? NoData(message: "Aucun utilisateurs")
            : Expanded(
                child: ListView.builder(
                  itemCount: users.length,
                  itemExtent: 62,
                  cacheExtent: 600,
                  itemBuilder: (context, id) {
                    PortailUser user = users[id];
                    return UserListItem(user: user);
                  },
                ),
              )
      ],
    );
  }
}
