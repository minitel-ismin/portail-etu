import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/logic/models/role.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/custom_view/custom_stateful_view.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';

class RolesListPage extends StatefulWidget {
  final AssociationData? asso;
  const RolesListPage({Key? key, this.asso}) : super(key: key);

  @override
  _RolesListPageState createState() => _RolesListPageState();
}

class _RolesListPageState extends State<RolesListPage> {
  @override
  Widget build(BuildContext context) {
    return CustomStatefulView(header: CustomHeader("Roles"), children: [
      Column(
        children: [
          for (Role r in widget.asso!.getRoles()
            ..sort((Role a, Role b) {
              if (a.hierarchy == null || b.hierarchy == null) {
                return 0;
              }

              return b.hierarchy!.compareTo(a.hierarchy!);
            }))
            ListTile(
              leading: CircleAvatar(
                child: Text(r.hierarchy.toString()),
              ),
              title: Text(r.name ?? 'null'),
              subtitle: Text(r.rights.length.toString() + " permissions"),
            ),
        ],
      ),
      Center(
        child: ElevatedButton(
            onPressed: () async {
              String? name = await CustomDialogs.showCustomTextDialog(
                context,
                title: "Nom du rôle",
                helpText: "nom",
                initialText: "",
              );
              if (name != null) {
                try {
                  await widget.asso!.createRole(name: name, hierarchy: 0);
                } catch (e) {
                  print("Could not create role");
                  print(e.toString());
                }

                setState(() {});
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Add role"),
            )),
      )
    ]);
  }
}
