import 'package:emse_bde_app/logic/models/role.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/custom_view/custom_stateful_view.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/icon_text.dart';
import 'package:emse_bde_app/partials/loading_bar.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';

class RolesDetailPage extends StatefulWidget {
  const RolesDetailPage({Key? key, required this.r}) : super(key: key);
  final Role r;

  @override
  _RolesDetailPageState createState() => _RolesDetailPageState();
}

class _RolesDetailPageState extends State<RolesDetailPage> {
  bool updating = false;
  Role? r;

  @override
  Widget build(BuildContext context) {
    r ??= widget.r;

    return CustomStatefulView(
        header: CustomHeader("Role " + r!.name!),
        children: [
          MaterialButton(
              child: H1Title(r!.name ?? ''),
              onPressed: () async {
                String? name = await CustomDialogs.showCustomTextDialog(
                  context,
                  title: "Changer le nom du rôle",
                  helpText: "nom",
                  initialText: r!.name.toString(),
                );
                if (name != null) {
                  r!.name = name;
                }
              }),
          MaterialButton(
              child: IconText(
                icon: Icons.sort,
                title: "Hierarchy",
                text: r!.hierarchy.toString(),
              ),
              onPressed: () async {
                int? hierarchy =
                    int.tryParse(await CustomDialogs.showCustomTextDialog(
                          context,
                          title: "Changer la valeur de la hiérarchie",
                          helpText: "Entre 0 et 10",
                          initialText: r!.hierarchy.toString(),
                        ) ??
                        '');
                if (hierarchy != null) {
                  r!.hierarchy = hierarchy;
                }
              }),
          if (updating) LoadingBar("Changing permissions"),
        ]);
  }
}
