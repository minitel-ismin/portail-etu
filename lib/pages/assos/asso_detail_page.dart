import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:file_picker/file_picker.dart';
import 'package:minestrix_chat/partials/dialogs/adaptative_dialogs.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/logic/models/logo.dart';
import 'package:emse_bde_app/logic/models/role.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/loading_bar.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/chat/settings/conv_settings_card.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';
import 'package:nanoid/nanoid.dart';

class AssoDetailPage extends StatefulWidget {
  const AssoDetailPage(this.assoData, {Key? key}) : super(key: key);
  final AssociationData? assoData;
  @override
  _AssoDetailPageState createState() => _AssoDetailPageState();
}

class _AssoDetailPageState extends State<AssoDetailPage> {
  bool uploadingFile = false;
  void setTitle() async {
    String? name = await CustomDialogs.showCustomTextDialog(context,
        title: "Changer le nom de l'asso",
        helpText: "Nom",
        initialText: widget.assoData!.name);
    if (name != null) {
      await widget.assoData!
          .setName(name); // if is matrix, will also change the name
      await widget.assoData!.waitForRoomSync();
      setState(() {});
    }
  }

  void setDescription() async {
    String? description = await CustomDialogs.showCustomTextDialog(
      context,
      title: "Changer la description de l'asso",
      helpText: "Description",
      initialText: widget.assoData!.description,
    );
    if (description != null) {
      // Edit the description

      await widget.assoData!.setDescription(description);
      await widget.assoData!.waitForRoomSync();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final AssociationData assoData = widget.assoData!;

    final mClient = Matrix.of(context).client;

    return StreamBuilder<Object>(
        stream: mClient.onSync.stream,
        builder: (context, snapshot) {
          return Scaffold(
              body: ListView(
            children: [
              CustomHeader(
                assoData.name,
              ),
              H1Title("Association settings"),
              Wrap(
                alignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 380),
                    child: Column(
                      children: [
                        ListTile(
                            title: Text("Nom de l'asso"),
                            subtitle: Text(assoData.name),
                            leading: Icon(Icons.title),
                            trailing: Icon(Icons.edit),
                            onTap: setTitle),
                        ListTile(
                            title: Text("Description de l'asso"),
                            subtitle: assoData.description != ""
                                ? Text(assoData.description)
                                : Text("Add description"),
                            leading: Icon(Icons.topic),
                            trailing: Icon(Icons.edit),
                            onTap: setDescription),
                        ListTile(
                            title: Text("Plus de paramètres"),
                            subtitle: Text("Editez les permissons"),
                            trailing: Icon(Icons.settings),
                            onTap: () async {
                              await AdaptativeDialogs.show(
                                  context: context,
                                  builder: (context) => ConvSettingsCard(
                                        room: widget.assoData!.r,
                                        onClose: () {},
                                      ));
                            }),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: uploadingFile
                        ? LoadingBar("Uploading file...")
                        : MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14))),
                            onPressed: () async {
                              FilePickerResult? result =
                                  await FilePicker.platform.pickFiles(
                                      type: FileType.image, withData: true);

                              if (result?.files.isNotEmpty == true &&
                                  result?.files.first.bytes == null) return;
                              setState(() {
                                uploadingFile = true;
                              });
                              String filename = "logo_asso_" + nanoid(10);
                              Logo? logo;
                              await assoData.r.setAvatar(MatrixFile(
                                  bytes: result!.files.first.bytes!,
                                  name: filename));

                              setState(() {
                                uploadingFile = false;
                                assoData.logo = logo?.filename;
                              });
                            },
                            child: SizedBox(
                              width: 200,
                              height: 200,
                              child: Stack(
                                children: [
                                  Center(
                                    child: MatrixImageAvatar(
                                      client: Matrix.of(context).client,
                                      url: assoData.avatar,
                                      width: 180,
                                      height: 180,
                                      defaultText: assoData.name,
                                      backgroundColor: Colors.blue,
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.topRight,
                                      child: IconButton(
                                          icon: Icon(Icons.delete),
                                          onPressed: () async {
                                            setState(() {
                                              uploadingFile = true;
                                            });

                                            await assoData.r.setAvatar(null);

                                            setState(() {
                                              assoData.logo = null;
                                              uploadingFile = false;
                                            });
                                          }))
                                ],
                              ),
                            ),
                          ),
                  ),
                ],
              ),
              ListTile(
                  title: Text("Roles list page"),
                  subtitle: Text("Ajoutez / supprimez des roles"),
                  trailing: Icon(Icons.nature_people),
                  onTap: () {
                    context.navigateTo(RolesListRoute(asso: assoData));
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  H2Title("Members"),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 14),
                    child: IconButton(
                        icon: Icon(Icons.group_add),
                        onPressed: () async {
                          Profile? p = await showDialog(
                              context: context,
                              builder: (context) => Dialog(
                                      child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      H1Title("Ajouter un membre"),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TypeAheadField<Profile>(
                                          textFieldConfiguration:
                                              TextFieldConfiguration(
                                                  autofocus: true,
                                                  style: DefaultTextStyle.of(
                                                          context)
                                                      .style
                                                      .copyWith(
                                                          fontStyle:
                                                              FontStyle.italic),
                                                  decoration: InputDecoration(
                                                      border:
                                                          OutlineInputBorder())),
                                          suggestionsCallback:
                                              (String pattern) async {
                                            SearchUserDirectoryResponse resp =
                                                await mClient
                                                    .searchUserDirectory(
                                                        pattern);
                                            Iterable<String>
                                                usersAlreadyInAsso = widget
                                                    .assoData!
                                                    .getPositionsFromMatrix()
                                                    .map((e) => e.user!.mxId!);

                                            return resp.results.where(
                                                // check if the user has not already been added
                                                (p) => !usersAlreadyInAsso
                                                    .contains(p.userId));
                                          },
                                          itemBuilder: (context, p) {
                                            return ListTile(
                                              leading: MatrixImageAvatar(
                                                client: mClient,
                                                url: p.avatarUrl,
                                                defaultText:
                                                    p.displayName ?? p.userId,
                                              ),
                                              title: Text(
                                                  p.displayName ?? p.userId),
                                              subtitle: Text(p.userId),
                                            );
                                          },
                                          onSuggestionSelected: (suggestion) {
                                            context.router
                                                .pop<Profile>(suggestion);
                                          },
                                        ),
                                      )
                                    ],
                                  )));
                          if (p != null) {
                            // check if postition doesn't exist !

                            assoData.addMember(p.userId, "");

                            setState(() {});
                          }
                        }),
                  ),
                ],
              ),
              Wrap(
                children: [
                  for (Position p in assoData.getPositionsFromMatrix())
                    ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 340),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Slidable(
                          child: Container(
                            child: ListTile(
                              leading: CircleAvatar(
                                backgroundColor: Colors.indigoAccent,
                                child: Icon(Icons.people),
                                foregroundColor: Colors.white,
                              ),
                              title: Text(
                                  p.user!.firstname! + " " + p.user!.lastname!),
                              subtitle: Text(p.role?.name ?? "no role defined"),
                              onTap: () async {
                                String? id = p.user?.mxId;
                                Role? r = await showDialog(
                                    context: context,
                                    builder: (context) => Dialog(
                                            child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            H1Title("Modifier le rôle"),
                                            Column(
                                              children: [
                                                Builder(builder: (context) {
                                                  List<Role> roles = widget
                                                      .assoData!
                                                      .getRoles();

                                                  List<DropdownMenuItem<Role>>
                                                      dropItemRoles = roles
                                                          .map<
                                                              DropdownMenuItem<
                                                                  Role>>((e) =>
                                                              DropdownMenuItem<
                                                                  Role>(
                                                                value: e,
                                                                child: Text(
                                                                    e.name!),
                                                              ))
                                                          .toList();

                                                  return Column(children: [
                                                    DropdownButton<Role>(
                                                      value: null,
                                                      items: dropItemRoles,
                                                      onChanged: (Role? value) {
                                                        context.router
                                                            .pop<Role>(value);
                                                      },
                                                    )
                                                  ]);
                                                }),
                                              ],
                                            )
                                          ],
                                        )));

                                if (r != null) {
                                  if (id != null)
                                    await widget.assoData!.setPositions(
                                        id, [r.matrixRoleID!], true);

                                  if (mounted) setState(() {});
                                }
                              },
                            ),
                          ),
                          endActionPane: ActionPane(
                            motion: ScrollMotion(),
                            children: <Widget>[
                              SlidableAction(
                                label: 'Supprimer',
                                backgroundColor: Colors.red,
                                icon: Icons.delete,
                                onPressed: (context) async {
                                  if (p.user?.mxId != null)
                                    assoData.deletePositions(p.user!.mxId!);

                                  setState(() {
                                    assoData.positions.remove(p);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                ],
              ),
              if (widget.assoData?.r.encrypted == true)
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                        color: Colors.green,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(Icons.health_and_safety),
                              SizedBox(
                                width: 12,
                              ),
                              Text("Encrypted room",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18)),
                            ],
                          ),
                        )),
                  ),
                ),
            ],
          ));
        });
  }
}
