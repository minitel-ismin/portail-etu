import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/partials/asso/asso_portail_card.dart';
import 'package:emse_bde_app/partials/asso/asso_portail_child.dart';
import 'package:emse_bde_app/partials/asso/matrix_asso_list_item.dart';
import 'package:emse_bde_app/partials/controls/custom_list.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart' as matrix;
import 'package:matrix/matrix.dart';
import 'package:matrix/src/utils/space_child.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class AssociationsPage extends StatefulWidget {
  const AssociationsPage({Key? key}) : super(key: key);

  @override
  _AssociationsPageState createState() => _AssociationsPageState();
}

class _AssociationsPageState extends State<AssociationsPage> {
  bool refreshing = false;

  Future<void> refresh(Client m) async {
    if (refreshing) return;
    setState(() {
      refreshing = true;
    });

    await m.joinAssoAndPortailChilds(); // auto join matrix room space childrens

    setState(() {
      refreshing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;
    return RefreshIndicator(
      onRefresh: () async {
        await refresh(m);
      },
      child: StreamBuilder(
          stream: m.onSync.stream,
          builder: (context, snap) => CustomList(
                title: "Les associations",
                hasData: !refreshing,
                actions: [
                  IconButton(
                      icon: !refreshing
                          ? Icon(Icons.refresh)
                          : CircularProgressIndicator(),
                      onPressed: () async {
                        if (!refreshing) await refresh(m);
                      }),
                ],
                children: [
                  Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 780),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for (matrix.Room r
                                    in m.getAssoRooms()
                                      ..sort(
                                          (a, b) => a.name.compareTo(b.name)))
                                  MatrixAssoListItem(
                                      assos: r,
                                      onPressed: () {
                                        AssociationData d =
                                            AssociationData.fromRoom(r);
                                        context.navigateTo(
                                            AssociationRoute(assoData: d));
                                      }),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Portail"),
                                for (matrix.Room r in m.getPortailsRooms())
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 22),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        AssoPortailCard(room: r),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: FutureBuilder<
                                                  matrix
                                                      .GetSpaceHierarchyResponse>(
                                              future: m.getSpaceHierarchy(r.id),
                                              builder: (context, snapshot) {
                                                if (snapshot.data == null)
                                                  return CircularProgressIndicator();

                                                final spaces =
                                                    snapshot.data!.rooms;

                                                return Column(
                                                  children: [
                                                    for (var space in spaces)
                                                      if (m.getRoomById(
                                                              space.roomId) ==
                                                          null) // get only unkown rooms
                                                        AssoPortailChild(
                                                            space: space,
                                                            onJoin: () =>
                                                                setState(() {}),
                                                            spaceChild: r
                                                                .spaceChildren
                                                                .firstWhereOrNull(
                                                                    (SpaceChild
                                                                            s) =>
                                                                        s.roomId ==
                                                                        space
                                                                            .roomId)),
                                                  ],
                                                );
                                              }),
                                        )
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )),
    );
  }
}
