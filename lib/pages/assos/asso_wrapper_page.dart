import 'package:auto_route/auto_route.dart';
import 'package:emse_bde_app/pages/assos/asso_page.dart';
import 'package:emse_bde_app/pages/assos/assos_list.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

class AssoWrapperPage extends StatefulWidget {
  const AssoWrapperPage({Key? key}) : super(key: key);

  @override
  State<AssoWrapperPage> createState() => AssoWrapperPageState();
}

class AssoWrapperPageState extends State<AssoWrapperPage> {
  bool mobile = false;

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => this,
      child: LayoutBuilder(builder: (context, constraints) {
        mobile = constraints.maxWidth < 800;
        return Row(
          children: [
            if (!mobile)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 400),
                      child: AssociationsPage()),
                ),
              ),
            Expanded(child: AutoRouter()),
          ],
        );
      }),
    );
  }
}

class AssoPlaceholderPage extends StatelessWidget {
  const AssoPlaceholderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, snap) {
      final state = Provider.of<AssoWrapperPageState>(context, listen: false);
      return state.mobile
          ? const AssociationsPage()
          : Center(child: Text("Mes assos"));
    });
  }
}
