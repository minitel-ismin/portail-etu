import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/matrix/extensions/room_extension.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/portail_state.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/event_item.dart';
import 'package:emse_bde_app/partials/helpers/text.dart';
import 'package:emse_bde_app/partials/matrix/chat_button.dart';
import 'package:emse_bde_app/partials/news/feed_widget.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:minestrix_chat/minestrix_chat.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:provider/provider.dart';

import 'asso_page_tabs/asso_info_tab.dart';
import 'asso_page_tabs/asso_mandat_tab.dart';

class AssociationPage extends StatefulWidget {
  const AssociationPage(this.assoData, {Key? key}) : super(key: key);

  final AssociationData assoData;

  @override
  _AssociationPageState createState() => _AssociationPageState();
}

class _AssociationPageState extends State<AssociationPage> {
  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.assoData != oldWidget.assoData) {
      assoData = null;
    }
  }

  AssociationData? assoData;

  Widget buildNavBar(context, Widget? actionButton) {
    return CustomHeader(widget.assoData.name,
        actionButton: actionButton != null ? [actionButton] : null);
  }

  /// Force cache refresh : will force joining the room child of this space

  Future<AssociationData?> getAssociationData(
      {bool forceRefresh: false}) async {
    if (assoData != null) return assoData;
    await widget.assoData.r.postLoad();
    assoData = widget.assoData;

    if (forceRefresh) {
      await assoData?.r.joinSpaceChilds();
    }

    return assoData;
  }

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return DefaultTabController(
        length: 4,
        child: Scaffold(
            body: Consumer<PortailState>(builder: (context, state, child) {
          return LayoutBuilder(builder: (context, constraints) {
            return StreamBuilder<Object>(
                stream: m.onSync.stream,
                builder: (context, snapshot) {
                  final relatedRooms = m.rooms.where((room) =>
                      widget.assoData.r.spaceChildren
                              .indexWhere((space) => space.roomId == room.id) !=
                          -1 &&
                      room.type == "");

                  return FutureBuilder<AssociationData?>(
                      future: getAssociationData(),
                      builder: (context, snapAssoData) {
                        return Column(
                          children: [
                            CustomHeader(widget.assoData.name, actionButton: [
                              if (assoData?.r.canSendDefaultStates == true)
                                IconButton(
                                    icon: Icon(Icons.info),
                                    onPressed: () async {
                                      await context.pushRoute(AssoDetailRoute(
                                          assoData: snapAssoData.data));
                                      await getAssociationData(
                                          forceRefresh: true);
                                      if (mounted) setState(() {});
                                    })
                            ]),
                            Expanded(
                              child: NestedScrollView(
                                headerSliverBuilder: (context, value) => [
                                  SliverToBoxAdapter(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Wrap(
                                      children: [
                                        Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: MatrixImageAvatar(
                                                client:
                                                    Matrix.of(context).client,
                                                url: widget.assoData.avatar,
                                                width: 180,
                                                height: 180,
                                                shape: MatrixImageAvatarShape
                                                    .rounded,
                                                defaultText:
                                                    widget.assoData.name,
                                                backgroundColor: Colors.blue)),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            if ([null, ""].contains(snapAssoData
                                                    .data?.description) ==
                                                false)
                                              Card(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        maxWidth: 500),
                                                    child: MarkdownBody(
                                                        data: snapAssoData.data
                                                                ?.description ??
                                                            ''),
                                                  ),
                                                ),
                                              ),
                                            SizedBox(height: 8),
                                            ChatButton(r: widget.assoData.r),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )),
                                  SliverToBoxAdapter(
                                      child: TabBar(
                                    tabs: [
                                      ListTile(
                                        title: Text(
                                          "Actualitées",
                                          maxLines: 1,
                                        ),
                                        leading: Icon(Icons.post_add),
                                      ),
                                      ListTile(
                                        title: Text(
                                          "Événèments",
                                          maxLines: 1,
                                        ),
                                        leading: Icon(Icons.event),
                                      ),
                                      ListTile(
                                        title: Text(
                                          "Mandat",
                                          maxLines: 1,
                                        ),
                                        leading: Icon(Icons.people),
                                      ),
                                      ListTile(
                                          title: Text(
                                            "Infos",
                                            maxLines: 1,
                                          ),
                                          leading: Icon(Icons.info))
                                    ],
                                  )),
                                ],
                                body: snapAssoData.hasData
                                    ? TabBarView(children: [
                                        Tab(
                                          child: ListView(children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: MatrixPostsWidget(
                                                key: Key(
                                                    snapAssoData.data!.r.id),
                                                room: snapAssoData.data!.r,
                                              ),
                                            ),
                                          ]),
                                        ),
                                        Tab(
                                          child: ListView(
                                            children: [
                                              Wrap(
                                                alignment: WrapAlignment.center,
                                                children: [
                                                  ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        maxWidth: 1000),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Column(
                                                          children: [
                                                            if (snapAssoData
                                                                    .data
                                                                    ?.events !=
                                                                null)
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: [
                                                                      H2Title(
                                                                          "Événèments"),
                                                                      if (assoData
                                                                              ?.r
                                                                              .canSendDefaultStates ==
                                                                          true)
                                                                        Padding(
                                                                          padding:
                                                                              const EdgeInsets.all(14),
                                                                          child: IconButton(
                                                                              icon: Icon(Icons.add),
                                                                              onPressed: () async {
                                                                                PortailEvent? e = await assoData!.createEvent();
                                                                                if (e != null) context.pushRoute(EventCreateRoute(event: e));
                                                                              }),
                                                                        ),
                                                                    ],
                                                                  ),
                                                                  snapAssoData
                                                                              .data!
                                                                              .events
                                                                              .length >
                                                                          0
                                                                      ? Center(
                                                                          child:
                                                                              Wrap(
                                                                            children: [
                                                                              for (PortailEvent event in snapAssoData.data!.events)
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(4),
                                                                                  child: EventItem(event, onNavigation: (e) => context.navigateTo(EventDetailRoute(event: e))),
                                                                                ),
                                                                            ],
                                                                          ),
                                                                        )
                                                                      : NoData(
                                                                          message:
                                                                              "Pas d'events"),
                                                                ],
                                                              ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Tab(
                                          child: ListView(
                                            children: [
                                              AssoMandatTab(
                                                  positions: snapAssoData
                                                      .data!.positions),
                                            ],
                                          ),
                                        ),
                                        Tab(
                                          child: ListView(
                                            children: [
                                              AssoInfoTab(
                                                  asso: snapAssoData.data!,
                                                  relatedRooms: relatedRooms),
                                            ],
                                          ),
                                        )
                                      ])
                                    : CircularProgressIndicator(),
                              ),
                            ),
                          ],
                        );
                      });
                });
          });
        })));
  }
}
