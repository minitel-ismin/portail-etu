import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/chat/settings/conv_settings_users.dart';
import 'package:minestrix_chat/partials/dialogs/adaptative_dialogs.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../logic/models/association_data.dart';
import '../../../partials/matrix/chat_button.dart';

class AssoInfoTab extends StatelessWidget {
  const AssoInfoTab({
    Key? key,
    required this.relatedRooms,
    required this.asso,
  }) : super(key: key);

  final AssociationData asso;
  final Iterable<Room> relatedRooms;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 500),
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.people),
                title: Text("Followers"),
                subtitle: Text("${asso.r.summary.mJoinedMemberCount}"),
                onTap: () {
                  AdaptativeDialogs.show(
                      context: context,
                      builder: (context) => ConvSettingsUsers(
                            room: asso.r,
                          ));
                },
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text("A propos"),
                subtitle: Text("${asso.r.topic}"),
              ),
              if (asso.isActive == false)
                Row(
                  children: [
                    Icon(Icons.archive),
                    SizedBox(width: 5),
                    Text("Archivé")
                  ],
                ),
              if (asso.r.lastEvent != null)
                ListTile(
                  leading: Icon(Icons.update),
                  title: Text("Mise à jour"),
                  subtitle: Text(
                      "${timeago.format(asso.r.lastEvent!.originServerTs)}"),
                ),
            ],
          ),
        ),
        for (final r in relatedRooms)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ChatButton(r: r, enableName: true),
          )
      ],
    );
  }
}
