import 'package:another_flushbar/flushbar.dart';
import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/portail_typo.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/controls/custom_file_upload.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/events/custom_edit/custom_edit_cost.dart';
import 'package:emse_bde_app/partials/events/custom_edit/custom_edit_title.dart';
import 'package:emse_bde_app/partials/events/event_form_creator.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';

class StoreItemCreatePage extends StatefulWidget {
  const StoreItemCreatePage({Key? key, required this.item}) : super(key: key);
  final StoreItem item;

  @override
  _StoreItemCreatePageState createState() => _StoreItemCreatePageState();
}

class _StoreItemCreatePageState extends State<StoreItemCreatePage> {
  late StoreItem event;
  late PortailEventBooking booking;

  bool uploadingFile = false;

  bool sending = false;

  @override
  void initState() {
    super.initState();
    event = widget.item;
    booking = event.eventBooking;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      CustomHeader("Modifier l'article"),
      Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        children: [
          SizedBox(
            width: 200,
            child: Center(
              child: CustomImageUploader(
                  room: event.r, onUpdate: () => setState(() {})),
            ),
          ),
          SizedBox(
            width: 700,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomEditTitle(event: event),
                CustomEditCost(booking: booking, onChanged: (e) => booking = e),
              ],
            ),
          ),
        ],
      ),
      Padding(
        padding: const EdgeInsets.all(16),
        child:
            EventFormCreator(booking: booking, onChanged: (e) => booking = e),
      ),
      CustomElevatedButton(
          message: "Modifier l'objet",
          icon: Icons.save,
          updating: sending,
          onPressed: () async {
            try {
              if (sending) return;
              setState(() {
                sending = true;
              });

              String? eText = event.eventBooking.getErrorMessage();

              if (eText != null) {
                setState(() {
                  sending = false;
                });
                await Flushbar(
                  title: 'Le formulaire comporte une erreur',
                  message: eText,
                  duration: Duration(seconds: 6),
                  icon: Icon(Icons.error, color: Colors.white),
                  backgroundColor: Colors.red,
                  leftBarIndicatorColor: Colors.red[800],
                ).show(context);
                return; // update the error message
              }

              await event.setEventBooking(booking);

              setState(() {
                sending = false;
              });
              // and navigate back

              if (Navigator.canPop(context)) context.router.pop();
            } catch (e) {
              print("Could not create item");
              print(e);
              setState(() {
                sending = false;
              });
            }
          }),
      Padding(
        padding: const EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PortailH2("Debug"),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CustomElevatedButton(
                  message: "Debug",
                  icon: Icons.bug_report,
                  color: Colors.red,
                  onPressed: () async {
                    context.pushRoute(EventCreateRoute(event: event));
                  }),
            ),
          ],
        ),
      )
    ]));
  }
}
