import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/matrix/extensions/event_extension.dart';

import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/store/store_fad_item.dart';
import 'package:emse_bde_app/partials/store/store_item.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class StorePage extends StatefulWidget {
  const StorePage({Key? key}) : super(key: key);

  @override
  _StorePageState createState() => _StorePageState();
}

class _StorePageState extends State<StorePage> {
  bool refreshing = false;
  bool creatingItem = false;

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    List<StoreItem> items = m.getStoreItems().toList();

    return StreamBuilder<Object>(
        stream: m.onSync.stream,
        builder: (context, snapshot) {
          return ListView(children: [
            CustomHeader("Articles", actionButton: [
              IconButton(
                  icon: creatingItem
                      ? CircularProgressIndicator()
                      : Icon(Icons.add),
                  onPressed: creatingItem
                      ? null
                      : () async {
                          setState(() {
                            creatingItem = true;
                          });
                          StoreItem? item =
                              await StoreItem.createNew(m, name: "", topic: "");
                          if (item != null)
                            await context
                                .pushRoute(StoreItemCreateRoute(item: item));
                          setState(() {
                            creatingItem = false;
                          });
                        })
            ]),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Wrap(
                children: [
                  for (StoreItem item in items)
                    FutureBuilder(
                        future: item.r.postLoad(),
                        builder: (context, snap) {
                          return MaterialButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: SizedBox(
                                width: 200,
                                height: 240,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: StoreFadButton(
                                      child: StoreItemWidget(item),
                                      onListPressed: () async {
                                        List<Booking> b = item.bookings;
                                        await context.navigateTo(
                                            EventBookingsRoute(bookings: b));
                                        setState(() {});
                                      },
                                      onDetailPressed: () async {
                                        await onDetailPressed(context, item);
                                      },
                                      onEditPressed: () async {
                                        await context.navigateTo(
                                            StoreItemCreateRoute(item: item));

                                        // download the new list from server and update UI
                                        setState(() {});
                                      },
                                      onBuyPressed: () async {
                                        await context.navigateTo(
                                            StoreItemCheckoutRoute(item: item));
                                      },
                                      canEdit: item.r
                                          .canSendDefaultStates), //TODO: fix me. Get power level for edition
                                ),
                              ),
                              onPressed: () async {
                                await onDetailPressed(context, item);
                              });
                        })
                ],
              ),
            )
          ]);
        });
  }

  Future<void> onDetailPressed(BuildContext context, StoreItem item) async {
    await context.navigateTo(StoreItemRoute(item: item));
  }
}
