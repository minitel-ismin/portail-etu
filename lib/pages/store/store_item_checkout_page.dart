import 'package:auto_route/auto_route.dart';
import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:flutter/material.dart';

class StoreItemCheckoutPage extends StatefulWidget {
  final StoreItem item;
  const StoreItemCheckoutPage({Key? key, required this.item}) : super(key: key);

  @override
  _StoreItemCheckoutPageState createState() => _StoreItemCheckoutPageState();
}

class _StoreItemCheckoutPageState extends State<StoreItemCheckoutPage> {
  bool sending = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(children: [
      CustomHeader("Checkout"),
      Center(
          child: Card(
              child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(widget.item.eventBooking.displayCost),
      ))),
      Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    sending
                        ? SizedBox(
                            height: 18,
                            width: 18,
                            child:
                                CircularProgressIndicator(color: Colors.white))
                        : Icon(Icons.send),
                    SizedBox(width: 10),
                    Text("Reserver"),
                  ],
                ),
              ),
              onPressed: sending
                  ? null
                  : () async {
                      await reserver();
                    }),
        ),
      )
    ]));
  }

  Future<void> reserver() async {
    setState(() {
      sending = true;
    });

    await widget.item.book();
    context.router.pop();

    if (mounted)
      setState(() {
        sending = false;
      });
  }
}
