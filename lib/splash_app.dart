import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'global/Managers/theme_manager.dart';

class SplashApp extends StatelessWidget {
  const SplashApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(builder: (context, theme, _) {
      return MaterialApp(theme: theme.theme, home: SplashAppPage());
    });
  }
}

class SplashAppPage extends StatelessWidget {
  const SplashAppPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image(
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              image: AssetImage("assets/images/login_background.jpg")),
          Center(
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image(
                      image: AssetImage('assets/images/piaf.jpg'),
                      width: 200,
                      height: 200,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text("Le PIAF",
                        style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey.shade900)),
                    Text("Le Portail Inter Assos Fédérées",
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey.shade900)),
                    SizedBox(
                      height: 30,
                    ),
                    CircularProgressIndicator()
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
