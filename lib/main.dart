import 'package:auto_route/auto_route.dart';
import 'package:cron/cron.dart';
import 'package:desktop_webview_window/desktop_webview_window.dart';
import 'package:emse_bde_app/global/Managers/theme_manager.dart';

import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:minestrix_chat/utils/login/login_extension.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/portail_state.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/utils/manager/client_manager.dart';
import 'package:provider/provider.dart';
import 'package:timetable/timetable.dart';

import 'logic/matrix/portail_matrix_types.dart';
import 'splash_app.dart';

void main(List<String> args) async {
  // enable time_machine
  WidgetsFlutterBinding.ensureInitialized();

  if (runWebViewTitleBarWidget(args)) {
    return;
  }

  ClientManager.importantStateEventsOverrides.addAll([
    EventTypes.RoomTopic,
    "m.room.type",
    PortailMatrixStateTypes.event,
    PortailMatrixStateTypes.form,
    PortailMatrixStateTypes.members,
    PortailMatrixStateTypes.role,
  ]);

  final clients = await ClientManager.getClients();

  runApp(ChangeNotifierProvider<PortailState>(
      create: (context) => PortailState(),
      child: ChangeNotifierProvider<ThemeNotifier>(
          create: (_) => new ThemeNotifier(), child: MyApp(clients: clients))));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key, required this.clients}) : super(key: key);

  final List<Client> clients;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _appRouter = AppRouter();
  Cron? cron;

  bool _initLock = false;

  Future<bool>? webLogin;

  Future<bool> initMatrix(Client m) async {
    Logs().i("[ logged ] : " + m.isLogged().toString());
    if (m.isLogged() == false && !_initLock) {
      _initLock = true;
      await m.init(
          waitForFirstSync: false, waitUntilLoadCompletedLoaded: false);
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Matrix(
        applicationName: 'Le PIAF',
        context: context,
        clients: widget.clients,
        child: Consumer<PortailState>(
          builder: (context, state, child) => StreamBuilder<LoginState?>(
              stream: Matrix.of(context).client.onLoginStateChanged.stream,
              builder: (context, AsyncSnapshot<LoginState?> mSnap) {
                Client client = Matrix.of(context).client;

                if (kIsWeb && client.shouldSSOLogin) {
                  webLogin ??= client.ssoLogin();
                }
                return FutureBuilder(
                    future: webLogin,
                    builder: (context, snapshot) {
                      // Progress indicator while login in on web
                      if (webLogin != null && !snapshot.hasData) {
                        return MaterialApp(
                          home: Scaffold(
                            body: Center(
                                child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: const [
                                CircularProgressIndicator(),
                                SizedBox(
                                  width: 12,
                                ),
                                Text("Loading")
                              ],
                            )),
                          ),
                        );
                      }

                      return FutureBuilder(
                          future: initMatrix(client),
                          builder: (context, snap) {
                            if (!snap.hasData) return SplashApp();

                            return buildApp(mSnap, state);
                          });
                    });
              }),
        ));
  }

  Widget buildApp(AsyncSnapshot<LoginState?>? mSnap, PortailState state) {
    return Consumer<ThemeNotifier>(builder: (context, theme, _) {
      return MaterialApp.router(
        localizationsDelegates: [TimetableLocalizationsDelegate()],
        routerDelegate: AutoRouterDelegate.declarative(
          _appRouter,
          routes: (_) {
            final c = Matrix.of(context).client;

            if (mSnap?.data == LoginState.loggedIn && cron == null) {
              cron = Cron();
              cron!.schedule(Schedule.parse('*/5 * * * *'), () async {
                print('every 5 minutes');
                await c.joinAssoAndPortailChilds();
              });
            }
            print("State : " + (mSnap?.data.toString() ?? "null"));
            return [
              if (c.isLogged() == true)
                AppWrapperRoute()
              // if they are not logged in, bring them to the Login page
              else
                WelcomeRouter(children: [WelcomeRoute()])
            ];
          },
        ),
        routeInformationParser: _appRouter.defaultRouteParser(),
        debugShowCheckedModeBanner: false,
        title: 'BDE My ISMIN',
        theme: theme.theme,
      );
    });
  }
}
