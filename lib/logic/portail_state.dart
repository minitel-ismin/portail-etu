import 'package:emse_bde_app/logic/cas_api.dart';
import 'package:flutter/material.dart';

class PortailState extends ChangeNotifier {
  bool loaded = false;

  // matrix
  String? matrixToken;

  PortailState() {
    print("Started portail state");
  }

  Future<bool> logInCAS(String username, String password, String server) async {
    matrixToken =
        await CASApi.login(username, password, server, matrixLogin: true);

    loaded = true;
    notifyListeners();
    return true;
  }

  Future<void> doLogout() async {
    notifyListeners();
  }
}
