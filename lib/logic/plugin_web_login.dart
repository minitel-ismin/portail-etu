import 'dart:html';

import 'package:emse_bde_app/logic/portail_state.dart';
import 'package:flutter/material.dart';

class WebLogin {
  static Widget? tryWebLogin(BuildContext context, PortailState state) {
    String wurl = window.location.href;

    Uri u = Uri.parse(wurl);

    String? matrixToken = u.queryParameters["loginToken"];

    if (matrixToken != null) {
      // we need to login to emse before
      state.matrixToken = matrixToken;
    }

    return null;
  }

  static void launchSame(String url) {
    window.location.href = url;
  }

  static String? getUrl() {
    return window.location.href;
  }
}
