class User {
  final int? id;
  final String? firstname;
  final String? lastname;
  final int? promo;
  final String? type; // ismin or icm
  String? login;

  String? mxId;

  String get displayName => firstname! + " " + lastname!;

  User(
      {this.id = 0,
      this.firstname,
      this.lastname,
      this.type = "matrix",
      required this.mxId,
      this.promo});

  User.fromJson(Map<String, dynamic> json, String base)
      : id = json["id"],
        firstname = json["firstname"],
        lastname = json["lastname"],
        promo = json["promo"],
        type = json["type"],
        login = json["login"] {
    mxId = getMatrixID(base);
  }

  String getMatrixID(String base) => "@" + login! + ":" + base;
}
