import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/minestrix_chat.dart';

class BaseModel {
  Room r;

  Timeline? t;
  Future<Timeline> getTimeline() async {
    if (t != null) return t!;
    t = await r.getTimeline();
    return t!;
  }

  String get roomId => r.id;
  String get name => r.displayname;

  Future<void> setName(String name) async {
    await r.setName(name);
  }

  Uri? get avatar => r.avatar;

  String get description => r.topic;
  Future<void> setDescription(String name) async {
    await r.setDescription(name);
  }

  List<Room> getSpaceChildrenWithType(String type) {
    List<Room> rooms = [];

    for (var sC in r.spaceChildren) {
      if (sC.roomId != null) {
        Room? _rChild = r.client.getRoomById(sC.roomId!);

        String? _type = _rChild?.getState("m.room.create")?.content["type"];
        if (_type == type) {
          rooms.add(_rChild!);
        }
      }
    }
    return rooms;
  }

  Future<void> waitForRoomSync() async {
    await r.waitForRoomSync();
  }

  BaseModel({required this.r});

  /// create the room, wait for first sync and set the différent power levels
  static Future<String> createRoom(Client c,
      {required String name,
      required String roomType,
      required String topic,
      String? secondType,
      Room? parentSpace,
      List<StateEvent>? states}) async {
    states ??= [];

    String roomId = await c.createRoom(
      creationContent: {"type": roomType},
      initialState: [
        if (parentSpace != null)
          StateEvent(type: "m.room.join_rules", content: {
            "join_rule": "restricted",
            "allow": [
              {
                "type": "m.room_membership",
                "room_id": parentSpace.id,
              }
            ]
          }),
        if (secondType != null)
          StateEvent(type: "m.room.type", content: {
            "type": secondType,
          }),
      ]..addAll(states),
      powerLevelContentOverride: {
        "events": {MatrixTypes.post: 50}
      },

      name: name,
      topic: topic,
      roomVersion:
          "8", // Set the default room version to 8 to benefit to restricted membership. TODO: Remove me when default room version will be >= 8
      visibility: Visibility.private,
    );

    // wait for sync
    await c.onSync.stream
        .firstWhere((sync) => sync.rooms?.join?.containsKey(roomId) ?? false);

    // and add room to space
    if (parentSpace != null) {
      await parentSpace.setSpaceChild(roomId);
    }
    return roomId;
  }
}
