class Logo {
  final int? id;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? filename;

  Logo.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        filename = json["filename"],
        createdAt = json["createdAt"] != null
            ? DateTime.parse(json["createdAt"])
            : null,
        updatedAt = json["updatedAt"] != null
            ? DateTime.parse(json["updatedAt"])
            : null;
}
