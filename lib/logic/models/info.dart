import 'package:emse_bde_app/logic/models/association_data.dart';

class Info {
  final int? id;
  String? mxId;
  final String? firstName;
  final String? lastName;
  final String? type;
  final String? login;
  final int? promo;
  final bool? contributeBDE;
  final List<dynamic>? eventsBooked;
  final List<String>? roles;
  final List<Position>? positions;

  Info.fromJson(Map<String, dynamic> json, String base)
      : id = json["id"],
        firstName = json["firstname"],
        lastName = json["lastname"],
        login = json["login"],
        type = json["type"],
        promo = json["promo"],
        contributeBDE = json["contributeBDE"],
        eventsBooked = json["eventsBooked"],
        positions = json["positions"] != null
            ? Position.listFromJson(json["positions"], base)
            : null,
        roles = json["roles"] != null
            ? json["roles"].map<String>((value) => value.toString()).toList()
            : null {
    mxId = getMatrixID(base);
  }

  String get displayName => firstName! + " " + lastName!;
  String getMatrixID(String base) => "@" + login! + ":" + base;
}
