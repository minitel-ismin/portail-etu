class ListHelpers {
  ListHelpers.fromJson(Map<String, dynamic> p);
  static List<ListHelpers> listFromJson(List<dynamic> values) {
    List<ListHelpers> rights = [];

    for (Map<String, dynamic> p in values) {
      rights.add(ListHelpers.fromJson(p));
    }
    return rights;
  }
}
