import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/base_model.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_item.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking_response.dart';
import 'package:matrix/matrix.dart';

class StoreItem extends BasePortailItem {
  StoreItem({required Room r})
      : super(r: r, type: PortailEventTypes.store_item);

  List<Booking> get bookings => []; // TODO: replace me

  /// Create the matrix event room
  static Future<StoreItem?> createNew(Client c,
      {required String name, required String topic, Room? parentSpace}) async {
    String roomId = await BaseModel.createRoom(c,
        roomType: PortailMatrixRoomTypes.storeItem,
        name: name,
        topic: topic,
        parentSpace: parentSpace);

    Room? r = c.getRoomById(roomId);
    if (r != null) {
      StoreItem i = StoreItem(r: r);
      return i;
    }
    return null;
  }

  Future<List<StoreItemReservation>> getReservations() async {
    List<PortailEventBookingResponse> responses = await getResponses();
    return responses.map((e) => StoreItemReservation(e)).toList();
  }

  Future<void> book() async {}
}

class StoreItemReservation {
  StoreItemReservation(PortailEventBookingResponse res) {}
}
