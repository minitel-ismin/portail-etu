import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/base_model.dart';
import 'package:emse_bde_app/logic/models/info.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/models/role.dart';
import 'package:emse_bde_app/logic/models/user.dart';
import 'package:minestrix_chat/utils/text.dart';
import 'package:matrix/matrix.dart' as m;

class AssociationData extends BaseModel {
  String tag = "";
  bool isList = false;
  String? type;
  bool? isActive;

  String? color;
  String? contrastColor;

  String? color2;
  String? contrastColor2;

  String? logo;

  List<Position> get positions => getPositionsFromMatrix();

  List<Position> getPositionsFromMatrix() {
    List<Position> p = [];

    List<Role> roles = getRoles();

    r.states[PortailMatrixStateTypes.members]?.forEach((key, e) {
      if (key.startsWith("_") == true) {
        String? position;
        if (e.content.containsKey("positions")) {
          List<dynamic> positions = e.content["positions"];
          for (String t in positions) {
            // TODO : Add all positions
            position = t;
          }
        }

        if (position != null) {
          String login = key
              .toString()
              .split(":")[0]
              .replaceAll("@", "")
              .replaceFirst("_",
                  ""); // we had to this as we are not allowed to change the state of an other user
          List<String> names = login.split(".");

          String? firstName = login;
          String? lastName = "";

          if (names.isNotEmpty) {
            firstName = names[0].toTitleCase();
          }
          if (names.length > 1) {
            lastName = names[1].toTitleCase();
          }
          User u = User(
              firstname: firstName,
              lastname: lastName,
              mxId: key.replaceFirst("_", ""));

          // get role from the state role list
          Role r = roles.firstWhere(
              (element) => element.matrixRoleID == position,
              orElse: () => Role(name: position, rights: []));

          Position _p = Position(id: 0, user: u, role: r);
          p.add(_p);
        }
      }
    });

    return p;
  }

  /// Create the matrix event room
  static Future<AssociationData?> createAsso(m.Client c,
      {required String name,
      required String topic,
      m.Room? parentSpace}) async {
    print("creating asso ");

    String roomId = await BaseModel.createRoom(c,
        roomType: "m.space",
        secondType: PortailMatrixRoomTypes.asso,
        name: name,
        topic: topic,
        parentSpace: parentSpace,
        states: []);
    m.Room? r = c.getRoomById(roomId);

    if (parentSpace != null) {
      await parentSpace.setSpaceChild(roomId);
    }

    if (r != null) {
      AssociationData a = AssociationData.fromRoom(r);

      // create base roles
      String baseId = "fr.emse.minitel.";
      await a.createRole(
          name: "Président", hierarchy: 10, id: baseId + "president");
      await a.createRole(
          name: "Vice-président", hierarchy: 9, id: baseId + "vice-president");
      await a.createRole(
          name: "Président", hierarchy: 8, id: baseId + "sec-gen");
      await a.createRole(
          name: "Vice-Trésorier", hierarchy: 7, id: baseId + "vice-tresorier");
      await a.createRole(
          name: "Trésorier", hierarchy: 8, id: baseId + "tresorier");
      await a.createRole(name: "Respo com", hierarchy: 2, id: baseId + "com");
      await a.createRole(
          name: "President", hierarchy: 1, id: baseId + "membre");
      await a.createRole(
          name: "Cotisant", hierarchy: 0, id: baseId + "cotisant");
      return a;
    }

    return null;
  }

  AssociationData.fromRoom(r) : super(r: r) {}

  List<PortailEvent> get events {
    List<m.Room> rooms = getSpaceChildrenWithType(PortailMatrixRoomTypes.event);
    return rooms.map((r) => PortailEvent(r: r)).toList();
  }

  @override
  bool operator ==(Object other) {
    if (other is AssociationData == false) return false;

    AssociationData o = other as AssociationData;

    return o.r.id == r.id;
  }

  static List<AssoType> types = [
    AssoType("familly", "Une famille"),
    AssoType("asso", "Une association"),
    AssoType("list", "Une liste")
  ];

  Future<void> addMember(String id, String position,
      {bool shouldWait: true}) async {
    await setPositions(id, [position], shouldWait);
  }

  Future<void> deletePositions(String id) async {
    await setPositions(id, [], true);
  }

  Future<void> setPositions(
      String id, List<String> positions, bool shouldWait) async {
    await r.client.setRoomStateWithKey(
      r.id,
      PortailMatrixStateTypes.members,
      "_" + id, // we are not allowed to change other user states
      {if (positions.isNotEmpty) "positions": positions},
    );
    if (shouldWait) await waitForRoomSync();
  }

  bool checkIfPositionExist(Info user) {
    for (Position p in positions) {
      if (p.user?.id == user.id) return true;
    }

    return false;
  }

  Future<PortailEvent?> createEvent() async {
    return await PortailEvent.createEvent(r.client,
        name: "New event and restricted", topic: "This time", parentSpace: r);
  }

  List<Role> getRoles() {
    List<Role> roles = [];
    r.states[PortailMatrixStateTypes.role]?.forEach((key, e) {
      Role role = Role.fromMatrix(matrixRoleID: key, event: e);

      if (role.name != null) roles.add(role); // a valid role must have a name
    });

    return roles;
  }

  Future<String?> createRole(
      {required String name, required int hierarchy, String? id}) async {
    id ??= "fr.emse.minitel." + name.toLowerCase().replaceAll(" ", "-");
    if (r.states[PortailMatrixStateTypes.role]?.containsKey(id) != true) {
      String event = await r.client.setRoomStateWithKey(
        r.id,
        PortailMatrixStateTypes.role,
        id,
        {"name": name, "hierarchy": hierarchy},
      );
      await waitForRoomSync();
      return event;
    } else {
      throw new Exception("Role " + id + " already exists");
    }
  }
}

class AssoType {
  String? tag;
  late String name;
  AssoType(String tag, String name) {
    this.tag = tag;
    this.name = name;
  }
}

class Position {
  final int? id;
  final User? user;
  Role? role; //
  bool isMatrix;

  Position({required this.id, required this.user, this.role}) : isMatrix = true;

  Position.fromJson(Map<String, dynamic> json, String base)
      : id = json["id"],
        user = json["user"] != null ? User.fromJson(json["user"], base) : null,
        role = json["role"] != null ? Role.fromJson(json["role"]) : null,
        isMatrix = false;

  static List<Position> listFromJson(List<dynamic>? values, String base) {
    List<Position> position = [];
    if (values != null)
      for (Map<String, dynamic> p in values) {
        position.add(Position.fromJson(p, base));
      }
    return position;
  }
}
