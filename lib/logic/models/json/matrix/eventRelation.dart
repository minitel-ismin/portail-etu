import 'package:json_annotation/json_annotation.dart';

part 'eventRelation.g.dart';

@JsonSerializable()
class EventRelation {
  EventRelation();

  String? event_id;
  String? rel_types;

  factory EventRelation.fromJson(Map<String, dynamic> json) =>
      _$EventRelationFromJson(json);

  Map<String, dynamic> toJson() => _$EventRelationToJson(this);
}
