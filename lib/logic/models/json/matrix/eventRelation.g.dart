// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eventRelation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventRelation _$EventRelationFromJson(Map<String, dynamic> json) =>
    EventRelation()
      ..event_id = json['event_id'] as String?
      ..rel_types = json['rel_types'] as String?;

Map<String, dynamic> _$EventRelationToJson(EventRelation instance) =>
    <String, dynamic>{
      'event_id': instance.event_id,
      'rel_types': instance.rel_types,
    };
