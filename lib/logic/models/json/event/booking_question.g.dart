// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking_question.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingQuestion _$BookingQuestionFromJson(Map<String, dynamic> json) =>
    BookingQuestion()
      ..id = json['id'] as String
      ..type = $enumDecode(_$QuestionTypeEnumMap, json['type'])
      ..max_selection = json['max_selection'] as int?
      ..question = json['question'] == null
          ? null
          : BookingQuestionTitle.fromJson(
              json['question'] as Map<String, dynamic>)
      ..optional = json['optional'] as bool
      ..answers = (json['answers'] as List<dynamic>)
          .map(
              (e) => BookingQuestionAnswers.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$BookingQuestionToJson(BookingQuestion instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$QuestionTypeEnumMap[instance.type]!,
      'max_selection': instance.max_selection,
      'question': instance.question?.toJson(),
      'optional': instance.optional,
      'answers': instance.answers.map((e) => e.toJson()).toList(),
    };

const _$QuestionTypeEnumMap = {
  QuestionType.title: 'title',
  QuestionType.text: 'text',
  QuestionType.choice: 'choice',
};

BookingQuestionTitle _$BookingQuestionTitleFromJson(
        Map<String, dynamic> json) =>
    BookingQuestionTitle()..title = json['title'] as String;

Map<String, dynamic> _$BookingQuestionTitleToJson(
        BookingQuestionTitle instance) =>
    <String, dynamic>{
      'title': instance.title,
    };

BookingQuestionAnswers _$BookingQuestionAnswersFromJson(
        Map<String, dynamic> json) =>
    BookingQuestionAnswers()
      ..id = json['id'] as String
      ..text = json['text'] as String
      ..cost = json['cost'] as int?;

Map<String, dynamic> _$BookingQuestionAnswersToJson(
        BookingQuestionAnswers instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'cost': instance.cost,
    };
