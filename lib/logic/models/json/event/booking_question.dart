import 'package:json_annotation/json_annotation.dart';

part 'booking_question.g.dart';

enum QuestionType { title, text, choice }

Map<QuestionType, String> OptionTypesNames = {
  QuestionType.title: "Titre",
  QuestionType.text: "Text",
  QuestionType.choice: "Choix"
};

@JsonSerializable(explicitToJson: true)
class BookingQuestion {
  String id = "null";
  QuestionType type = QuestionType.choice;
  int? max_selection;
  BookingQuestionTitle? question;
  bool optional = true;

  List<BookingQuestionAnswers> answers = [];

  BookingQuestion();
  factory BookingQuestion.fromJson(Map<String, dynamic> json) =>
      _$BookingQuestionFromJson(json);

  Map<String, dynamic> toJson() => _$BookingQuestionToJson(this);
}

@JsonSerializable()
class BookingQuestionTitle {
  BookingQuestionTitle();
  String title = "";

  factory BookingQuestionTitle.fromJson(Map<String, dynamic> json) =>
      _$BookingQuestionTitleFromJson(json);

  Map<String, dynamic> toJson() => _$BookingQuestionTitleToJson(this);
}

@JsonSerializable()
class BookingQuestionAnswers {
  String id = "null";
  String text = "";
  int? cost; // prix en centime

  BookingQuestionAnswers();

  factory BookingQuestionAnswers.fromJson(Map<String, dynamic> json) =>
      _$BookingQuestionAnswersFromJson(json);

  Map<String, dynamic> toJson() => _$BookingQuestionAnswersToJson(this);
}
