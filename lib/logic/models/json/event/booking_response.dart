import 'package:emse_bde_app/logic/models/json/event/matrix_event_int.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:matrix/matrix.dart';

part 'booking_response.g.dart';

@JsonSerializable()
class BookingResponse {
  String id = "";
  List<String> answers = [];

  BookingResponse();

  factory BookingResponse.fromJson(Map<String, dynamic> json) =>
      _$BookingResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BookingResponseToJson(this);
}

@JsonSerializable()
class BookingResponseValidation extends MatrixEventInt {
  bool validated = false;

  BookingResponseValidation();

  factory BookingResponseValidation.fromEvent(Event event) {
    return _$BookingResponseValidationFromJson(event.content)..e = event;
  }

  Map<String, dynamic> toJson() => _$BookingResponseValidationToJson(this);
}
