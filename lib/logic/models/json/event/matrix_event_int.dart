import 'package:json_annotation/json_annotation.dart';
import 'package:matrix/matrix.dart';

class MatrixEventInt {
  @JsonKey(ignore: true)
  Event? e;
  MatrixEventInt();
}
