// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'portail_event_description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PortailEventDescription _$PortailEventDescriptionFromJson(
        Map<String, dynamic> json) =>
    PortailEventDescription()
      ..location = json['location'] as String?
      ..timeStart = json['time_start'] == null
          ? null
          : DateTime.parse(json['time_start'] as String)
      ..timeEnd = json['time_end'] == null
          ? null
          : DateTime.parse(json['time_end'] as String);

Map<String, dynamic> _$PortailEventDescriptionToJson(
        PortailEventDescription instance) =>
    <String, dynamic>{
      'location': instance.location,
      'time_start': instance.timeStart?.toIso8601String(),
      'time_end': instance.timeEnd?.toIso8601String(),
    };
