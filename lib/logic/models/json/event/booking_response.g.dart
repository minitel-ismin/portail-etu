// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingResponse _$BookingResponseFromJson(Map<String, dynamic> json) =>
    BookingResponse()
      ..id = json['id'] as String
      ..answers =
          (json['answers'] as List<dynamic>).map((e) => e as String).toList();

Map<String, dynamic> _$BookingResponseToJson(BookingResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'answers': instance.answers,
    };

BookingResponseValidation _$BookingResponseValidationFromJson(
        Map<String, dynamic> json) =>
    BookingResponseValidation()..validated = json['validated'] as bool;

Map<String, dynamic> _$BookingResponseValidationToJson(
        BookingResponseValidation instance) =>
    <String, dynamic>{
      'validated': instance.validated,
    };
