import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking_response.dart';
import 'package:matrix/matrix.dart';

class Booking {
  PortailEventBooking booking;
  PortailEventBookingResponse response;

  bool get isChecked =>
      false; // return wether or not this booking was confirmed by an admin member of the event.
  User get user => response.e!.sender;
  DateTime get createdAt => response.e!.originServerTs;

  Booking({required this.booking, required this.response})
      : assert(response.e != null);

  int get cost {
    return 0;
  }
}
