import 'package:json_annotation/json_annotation.dart';

part 'portail_event_description.g.dart';

@JsonSerializable()
class PortailEventDescription {
  String? location;

  @JsonKey(name: "time_start")
  DateTime? timeStart;

  @JsonKey(name: "time_end")
  DateTime? timeEnd;

  PortailEventDescription();

  factory PortailEventDescription.fromJson(Map<String, dynamic> json) =>
      _$PortailEventDescriptionFromJson(json);

  Map<String, dynamic> toJson() => _$PortailEventDescriptionToJson(this);

  String? getErrorMessage() {
    if (timeStart == null)
      return "Veuillez sélectionner une date de début pour l'événement !";
    if (timeEnd == null || timeEnd!.compareTo(timeStart!) <= 0)
      return "Veuiller sélectionner une date de fin valide pour l'évènement !. La date de fin de l'évènement doit être après le début de l'évènement.";

    return null;
  }
}
