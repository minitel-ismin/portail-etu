import 'package:json_annotation/json_annotation.dart';

part 'portailUser.g.dart';

@JsonSerializable()
class PortailUser {
  String? about;
  String userID;
  String? promo;
  DateTime? birthday;

  PortailUser(String userID) : userID = userID;

  factory PortailUser.fromJson(Map<String, dynamic> json,
      {required String key}) {
    PortailUser u = _$PortailUserFromJson(json);
    u.userID = key;
    return u;
  }

  Map<String, dynamic> toJson() => _$PortailUserToJson(this);
}
