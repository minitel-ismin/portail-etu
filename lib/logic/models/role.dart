import 'package:matrix/matrix.dart' as m;

class Role {
  final int? id;
  String? matrixRoleID; // the matrix role id

  int? hierarchy;
  String? name;
  String? description;

  m.Event? event;

  // may be depreciated in future release
  final List<Right> rights;

  Role({this.id: 0, required this.name, required this.rights});

  Role.fromMatrix({this.matrixRoleID, this.event})
      : id = 0,
        rights = [] {
    Map<String, dynamic> content = event!.content;
    name = content["name"];
    description = content["description"];
    hierarchy = content["hierarchy"] is int ? content["hierarchy"] : 0;
  } // TODO: replace the id

  Role.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        hierarchy = json["hierarchy"],
        rights = Right.listFromJson(json["rights"]);

  static List<Role> listFromJson(List<dynamic>? values) {
    List<Role> roles = [];
    if (values != null)
      for (Map<String, dynamic> p in values) {
        roles.add(Role.fromJson(p));
      }
    return roles;
  }
}

class Right {
  final int? id;
  final String? name;
  final String? description;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  Right.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        description = json["description"],
        createdAt = json["createdAt"] != null
            ? DateTime.parse(json["createdAt"])
            : null,
        updatedAt = json["updatedAt"] != null
            ? DateTime.parse(json["updatedAt"])
            : null;

  static List<Right> listFromJson(List<dynamic>? values) {
    List<Right> rights = [];
    if (values != null)
      for (Map<String, dynamic> p in values) {
        rights.add(Right.fromJson(p));
      }
    return rights;
  }

  @override
  bool operator ==(Object other) {
    return (other is Right) && other.id == id;
  }
}
