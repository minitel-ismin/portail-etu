import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';

import 'package:web_scraper/web_scraper.dart';

class CASApi {
  static Future<String?> login(String username, String password, String service,
      {bool matrixLogin: false}) async {
    // get lt and execution parameters
    var serviceParameters = {"service": service};

    // log in
    var dio = new Dio();
    final webScraper = WebScraper();

    CookieManager manager = CookieManager(CookieJar());
    dio.interceptors.add(manager);

    Map<String, dynamic> queryParameters = {
      "_eventId": "submit",
      "username": username,
      "password": password
    };

    Options dioOptions = Options(
        contentType: Headers.formUrlEncodedContentType,
        validateStatus: (status) {
          // don't throw an error on 302 redirect
          return status! < 500;
        });

    Map<String, String> redirectToCASQueryParameters = {};
    String casURL =
        "https://matrix.emse.fr/_matrix/client/r0/login/sso/redirect/saml";
    redirectToCASQueryParameters["redirectUrl"] = service;

    var redirectToCAS = await dio.get(casURL,
        options: dioOptions, queryParameters: redirectToCASQueryParameters);

    if (await webScraper.loadFromString(redirectToCAS.data)) {
      List<Map<String, dynamic>> elements =
          webScraper.getElement('input', ['name', 'value']);
      for (Map<String, dynamic> p in elements) {
        //print(p);
        if (p["attributes"]["name"] == "execution")
          queryParameters["execution"] = p["attributes"]["value"];
      }
    }

    queryParameters["service"] =
        redirectToCAS.realUri.queryParameters["service"];
    if (matrixLogin)
      queryParameters["entityId"] =
          redirectToCAS.realUri.queryParameters["entityId"];

    // login
    var resp = await dio.post(
        "https://cas.emse.fr" + redirectToCAS.realUri.path,
        options: dioOptions,
        queryParameters: serviceParameters,
        data: queryParameters);
    if (resp.data != "") print(resp.data);

    if (resp.headers["location"] == null) return null;
    Uri matrixUri = Uri.parse(resp.headers["location"]![0].toString());

    var matrixRequest = await dio.get(matrixUri.origin + matrixUri.path,
        options: dioOptions, queryParameters: matrixUri.queryParameters);

    Map<String?, String?> matrixAuthQueryParameters = {};
    if (await webScraper.loadFromString(matrixRequest.data)) {
      List<Map<String, dynamic>> elements =
          webScraper.getElement('input', ['name', 'value']);
      for (Map<String, dynamic> p in elements) {
        matrixAuthQueryParameters[p["attributes"]["name"]] =
            p["attributes"]["value"];
      }

      String matrixAuthPath =
          webScraper.getElement('form', ['action'])[0]["attributes"]["action"];

      var matrixAuth = await dio.post(matrixAuthPath,
          options: dioOptions, data: matrixAuthQueryParameters);

      var matrixQ = Uri.parse(matrixAuth.headers["location"]![0]);
      return matrixQ.queryParameters["loginToken"];
    }

    if (resp.headers["location"] != null) {
      var dest = resp.headers["location"]![0];
      Uri uri_ticket = Uri.parse(dest);

      return uri_ticket.queryParameters["ticket"];
    }

    return null;
  }
}
