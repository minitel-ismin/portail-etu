import 'dart:convert';
import 'package:http/http.dart' as http;

class API {
  static String server = "myismin.emse.fr";
  static String baseUrl = "https://" + server + "/api/";

  static Future<Map<String, dynamic>?> login(
      String ticket, String service) async {
    print(ticket);
    var queryParameters = {'ticket': ticket, 'service': service};

    Uri uri = Uri.https(server, "/api/login", queryParameters);
    var response = await http.get(uri);

    print(response.statusCode);
    print(response.body);

    return jsonDecode(response.body);
  }
}
