import 'package:emse_bde_app/logic/matrix/extensions/portail_extension.dart';
import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/models/event/rental_item.dart';
import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:matrix/matrix.dart';

extension MatrixEventExtension on Client {
  Iterable<PortailEvent> getAssoEvents() {
    List<Room> _rooms = rooms
        .where((r) =>
            r.getState(EventTypes.RoomCreate)?.content["type"] ==
            PortailMatrixRoomTypes.event)
        .toList();

    return _rooms.map((r) => PortailEvent(r: r));
  }

  Iterable<RentalItem> getRentalItems() =>
      filterRoomWithType(PortailMatrixRoomTypes.rentalItem)
          .map((r) => RentalItem(r: r));
  Iterable<StoreItem> getStoreItems() =>
      filterRoomWithType(PortailMatrixRoomTypes.storeItem)
          .map((r) => StoreItem(r: r));
}
