import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/minestrix_chat.dart';

extension MatrixRoomFeedExtension on Room {
  List<Event> filterEvents(List<Event> _events) => _events
      .where((e) =>
          !{
            RelationshipTypes.edit,
            RelationshipTypes.reaction,
            RelationshipTypes.reply,
          }.contains(e.relationshipType) &&
          {EventTypes.Message, EventTypes.Encrypted, MatrixTypes.post}
              .contains(e.type) &&
          !e.redacted)
      .toList();

  Future<List<Event>> getNews() async {
    List<Event> feed = [];

    Timeline t = await getTimeline();

    List<Event> filteredEvents;

    while ((filteredEvents = filterEvents(t.events)).length == 0 &&
        t.canRequestHistory &&
        false) {
      print("req");
      await t.requestHistory();
    }

    for (Event e in filteredEvents) {
      var eventEdit = e.getDisplayPostEvent(t); // event.getDisplayEvent(t);

      String eventText = eventEdit.content['m.text'] is String
          ? eventEdit.content['m.text']
          : eventEdit.body;
      if (eventText != "") feed.add(eventEdit);
    }

    return feed;
  }
}
