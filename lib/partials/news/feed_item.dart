import 'package:emse_bde_app/logic/matrix/portail_matrix_types.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/config/matrix_types.dart';
import 'package:minestrix_chat/minestrix_chat.dart';
import 'package:minestrix_chat/partials/feed/posts/matrix_post_content.dart';
import 'package:minestrix_chat/partials/feed/posts/matrix_post_editor.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:timeago/timeago.dart' as timeago;

class FeedItem extends StatelessWidget {
  const FeedItem(this.e, {Key? key}) : super(key: key);
  final Event e;

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return Padding(
      padding: const EdgeInsets.all(4),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      MatrixImageAvatar(
                          client: m,
                          width: 42,
                          height: 42,
                          url: e.room.avatar,
                          defaultText: e.room.name,
                          backgroundColor: Theme.of(context).primaryColor),
                      SizedBox(width: 8),
                      Expanded(
                        child: Text(e.room.name,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600)),
                      ),
                      if (e.room.type == PortailMatrixRoomTypes.event)
                        Text("Event"),
                      if (e.room.subtype == PortailMatrixRoomTypes.asso)
                        Text("Asso"),
                      if (e.senderId == m.userID &&
                          e.room.canSendEvent(MatrixTypes.post))
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: MaterialButton(
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                children: [
                                  Icon(Icons.edit,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onPrimary),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text("Edit",
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onPrimary)),
                                ],
                              ),
                              onPressed: () async {
                                await showDialog(
                                    context: context,
                                    builder: (context) => Dialog(
                                          child: ConstrainedBox(
                                            constraints: BoxConstraints(
                                                maxWidth: 800, maxHeight: 800),
                                            child: PostEditorPage(
                                                room: [e.room], post: e),
                                          ),
                                        ));
                              }),
                        )
                    ],
                  ),
                  SizedBox(height: 8),
                  MatrixPostContent(
                    event: e,
                    onImagePressed: (Event e,
                        {Event? imageEvent, String? ref}) {
                      //TODO: add handler
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      MatrixImageAvatar(
                          client: m,
                          width: 24,
                          height: 24,
                          url: e.sender.avatarUrl,
                          defaultText: e.sender.displayName ?? e.senderId,
                          backgroundColor: Theme.of(context).primaryColor),
                      SizedBox(width: 8),
                      Text(e.sender.displayName ?? e.senderId),
                      Text(" | "),
                      Text(timeago.format(e.originServerTs),
                          style: Theme.of(context).textTheme.caption),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }
}
