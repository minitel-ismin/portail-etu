import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/controls/custom_text_field.dart';
import 'package:flutter/material.dart';

class CustomEditCost extends StatelessWidget {
  final PortailEventBooking booking;
  final Function(PortailEventBooking) onChanged;
  const CustomEditCost(
      {Key? key, required this.booking, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
        name: "Prix",
        text: ((booking.cost ?? 0).toDouble() / 100).toString(),
        number: true,
        onChanged: (double? value) {
          if (value != null) {
            int val = (value * 100).toInt();
            booking.cost = val;
            onChanged(booking);
          }
        });
  }
}
