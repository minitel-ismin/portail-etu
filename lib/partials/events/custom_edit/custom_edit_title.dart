import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/partials/controls/custom_file_upload.dart';
import 'package:flutter/material.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';

class CustomEditTitle extends StatefulWidget {
  final BasePortailEvent event;
  const CustomEditTitle({Key? key, required this.event}) : super(key: key);

  @override
  _CustomEditTitleState createState() => _CustomEditTitleState();
}

class _CustomEditTitleState extends State<CustomEditTitle> {
  @override
  Widget build(BuildContext context) {
    BasePortailEvent event = widget.event;
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 60.0, vertical: 20),
          child: CustomImageUploader(room: widget.event.r),
        ),
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 400),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                  title: Text("Nom l'item"),
                  subtitle: Text(event.name),
                  trailing: Icon(Icons.edit),
                  onTap: () async {
                    String? name = await CustomDialogs.showCustomTextDialog(
                        context,
                        title: "Changer le nom de l'asso",
                        helpText: "Nom",
                        initialText: event.name);
                    if (name != null) {
                      await event.setName(
                          name); // if is matrix, will also change the name
                      await event.waitForRoomSync();
                      setState(() {});
                    }
                  }),
              ListTile(
                  title: Text("Description de l'item"),
                  trailing: Icon(Icons.edit),
                  subtitle: event.description != ""
                      ? Text(event.description)
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.edit),
                            SizedBox(width: 5),
                            Text("Add description"),
                          ],
                        ),
                  onTap: () async {
                    String? description =
                        await CustomDialogs.showCustomTextDialog(
                      context,
                      title: "Changer la description de l'asso",
                      helpText: "Description",
                      initialText: event.description,
                    );
                    if (description != null) {
                      // Edit the description

                      await event.setDescription(description);
                      await event.waitForRoomSync();
                      setState(() {});
                    }
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
