import 'package:another_flushbar/flushbar.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/controls/custom_switch_listTile.dart';
import 'package:emse_bde_app/partials/controls/custom_text_field.dart';
import 'package:emse_bde_app/partials/events/custom_edit/custom_edit_cost.dart';
import 'package:emse_bde_app/partials/helpers/date_selector.dart';
import 'package:flutter/material.dart';

class EventEditBooking extends StatefulWidget {
  final PortailEvent event;
  final VoidCallback? onSave;
  const EventEditBooking({Key? key, required this.event, this.onSave})
      : super(key: key);

  @override
  _EventEditBookingState createState() => _EventEditBookingState();
}

class _EventEditBookingState extends State<EventEditBooking> {
  late PortailEventBooking booking;
  bool bookingEdited = false;
  @override
  void initState() {
    super.initState();
    booking = widget.event.eventBooking;
  }

  Future<void> save() async {
    if (bookingEdited) {
      if (booking.getErrorMessage() == null) {
        await widget.event.setEventBooking(booking);
        bookingEdited = false;

        setState(() {});
        widget.onSave?.call();
      } else {
        await Flushbar(
          title: "Les détails de l'évènement comportent une erreur",
          message: booking.getErrorMessage(),
          duration: Duration(seconds: 6),
          icon: Icon(Icons.error, color: Colors.white),
          backgroundColor: Colors.red,
          leftBarIndicatorColor: Colors.red[800],
        ).show(context);
      }
    }
  }

  void onChanged() {
    setState(() {
      bookingEdited = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 50),
      child: ListView(
        children: [
          Card(
              color: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    CustomSwitchListTile(
                        title: "Évènement avec réservation ?",
                        value: booking.isBookable,
                        color: Colors.white,
                        onChanged: (value) async {
                          booking.isBookable = value;
                          onChanged();
                        }),
                  ],
                ),
              )),
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Column(
              children: [
                CustomEditCost(
                    booking: booking,
                    onChanged: (value) {
                      booking = value;
                      onChanged();
                    }),
                if (booking.isBookable)
                  DateSelector(
                      name: "Deadline de réservation",
                      date: booking.bookingTimeEnd,
                      onNewDate: (date) async {
                        booking.bookingTimeEnd = date;
                        onChanged();
                      }),
              ],
            ),
          ),
          Card(
              color: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    CustomSwitchListTile(
                        title: "Évènement au shotgun ?",
                        value: booking.shotgunListLength != 0,
                        color: Colors.white,
                        onChanged: (value) async {
                          booking.shotgunListLength = value ? 1 : 0;

                          onChanged();
                        }),
                  ],
                ),
              )),
          if (booking.shotgunListLength != 0)
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Column(
                children: [
                  CustomTextField(
                      name: "Nombre de places au shotgun",
                      text: booking.shotgunListLength.toString(),
                      number: true,
                      onChanged: (value) {
                        booking.shotgunListLength = value.toInt();
                        onChanged();
                      }),
                  DateSelector(
                      name: "Date du début du shotgun",
                      date: booking.bookingTimeStart,
                      onNewDate: (date) {
                        booking.bookingTimeStart = date;
                        onChanged();
                      }),
                ],
              ),
            ),
          if (bookingEdited)
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: CustomElevatedButton(
                icon: Icons.edit,
                message: "Sauvegarder",
                onFuturePressed: () => save(),
              ),
            )
        ],
      ),
    );
  }
}
