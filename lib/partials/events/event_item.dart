import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:intl/intl.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart' as m;
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/minestrix_chat.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

const width = 360.0;

class EventItem extends StatefulWidget {
  final AsyncCallback? onResult;
  final Function(PortailEvent)? onNavigation;
  final PortailEvent e;

  EventItem(this.e, {this.onResult, this.onNavigation});

  @override
  _EventItemState createState() => _EventItemState();
}

class _EventItemState extends State<EventItem> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    PortailEvent e = widget.e;
    final room = e.r;

    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      padding: const EdgeInsets.all(4),
      onPressed: () async {
        if (_loading == false) {
          setState(() {
            _loading = true;
          });

          if (room.membership == Membership.invite) await room.join();

          widget.onNavigation == null
              ? await context.navigateTo(EventDetailRoute(event: e))
              : widget.onNavigation!(e);

          if (widget.onResult != null) {
            await widget.onResult!();
          }

          if (mounted)
            setState(() {
              _loading = false;
            });
        }
      },
      child: Stack(
        children: [
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: [
                Container(
                  height: 180,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: (e.avatar != null)
                          ? CachedNetworkImageProvider(e.avatar!
                              .getThumbnail(Matrix.of(context).client,
                                  height: 300,
                                  width: 300,
                                  method: m.ThumbnailMethod.scale,
                                  animated: true)
                              .toString())
                          : AssetImage('assets/images/event_placeholder.jpg')
                              as ImageProvider,
                    ),
                  ),
                  child: Stack(
                    children: [
                      if (e.eventDescription.timeStart != null)
                        Positioned(
                          left: 10,
                          bottom: 10,
                          child:
                              DateWidget(date: e.eventDescription.timeStart!),
                        ),
                      Positioned(
                        right: 10,
                        bottom: 10,
                        child: Card(
                          color: Colors.green,
                          child: Padding(
                            padding: const EdgeInsets.all(4),
                            child: Column(
                              children: [
                                Text("Sur réservation",
                                    style: TextStyle(
                                        fontSize: 13.5, color: Colors.white)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 130,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(child: EventCreator(portailEvent: e)),
                            if (room.membership == Membership.invite)
                              Card(
                                color: Theme.of(context).primaryColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text("Invited",
                                      style: TextStyle(
                                          fontSize: 13.5,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onPrimary)),
                                ),
                              ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              e.name,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            if (!{null, ""}
                                .contains(e.eventDescription.location))
                              Flexible(
                                  child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 2.0, left: 4),
                                    child: Icon(Icons.place, size: 13),
                                  ),
                                  Flexible(
                                      child: Text(e.eventDescription.location!,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis)),
                                ],
                              )),
                          ],
                        ),
                        Expanded(
                            child: Text(
                          e.description,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ))
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          if (_loading)
            SizedBox(
              width: 260,
              height: 220,
              child: Center(
                  child: CircularProgressIndicator(
                color: Colors.white,
              )),
            )
        ],
      ),
    );
  }
}

class DateWidget extends StatelessWidget {
  const DateWidget({
    Key? key,
    required this.date,
  }) : super(key: key);

  final DateTime date;

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Theme.of(context).primaryColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text(DateFormat.MMM().format(date),
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16)),
              const SizedBox(height: 3),
              Text(DateFormat.d().format(date),
                  style: const TextStyle(fontSize: 22))
            ],
          ),
        ));
  }
}

class EventCreator extends StatelessWidget {
  const EventCreator({
    Key? key,
    required this.portailEvent,
  }) : super(key: key);

  final PortailEvent portailEvent;

  @override
  Widget build(BuildContext context) {
    final creator = portailEvent.r.creator;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          const Text("Event by"),
          const SizedBox(width: 8),
          SizedBox(
            height: 26,
            width: 26,
            child: MatrixImageAvatar(
              url: creator?.avatarUrl,
              client: portailEvent.r.client,
              defaultText: creator?.displayName ?? creator?.id,
              textPadding: 4,
            ),
          ),
          const SizedBox(width: 4),
          Text(creator?.calcDisplayname() ?? "",
              style: const TextStyle(fontWeight: FontWeight.bold))
        ],
      ),
    );
  }
}
