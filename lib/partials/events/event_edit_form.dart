import 'package:emse_bde_app/logic/models/json/event/booking_question.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_booking.dart';
import 'package:emse_bde_app/partials/events/event_form_creator.dart';
import 'package:flutter/material.dart';

class EventEditForm extends StatelessWidget {
  final Function(PortailEventBooking) onChanged;
  final PortailEventBooking booking;
  const EventEditForm(
      {Key? key, required this.booking, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Formulaire",
                          style: TextStyle(
                            fontSize: 20,
                          )),
                    ),
                    EventFormCreator(booking: booking, onChanged: onChanged),
                    if (booking.form_questions.length > 0)
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: ElevatedButton(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(Icons.add),
                                  SizedBox(width: 5),
                                  Text("Ajouter un champs ?"),
                                ],
                              ),
                            ),
                            onPressed: () {
                              BookingQuestion option = BookingQuestion();

                              booking.form_questions.add(option);
                              onChanged(booking);
                            }),
                      ),
                  ]),
            ),
          ),
        ),
      ],
    );
  }
}
