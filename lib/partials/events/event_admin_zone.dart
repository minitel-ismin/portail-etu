import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/booking.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';

class EventAdminZone extends StatelessWidget {
  const EventAdminZone(
      {Key? key, required this.event, this.eventLoaded: true, this.onRefresh})
      : super(key: key);

  final PortailEvent event;
  final bool eventLoaded;
  final VoidCallback? onRefresh;

  @override
  Widget build(BuildContext context) {
    String itemType = "event";
    if (event.type == PortailEventTypes.store_item) itemType = "article";
    if (event.type == PortailEventTypes.reservation) itemType = "reservation";

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14),
          child:
              Text("Admin zone", style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Wrap(
            children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: eventLoaded
                    ? ElevatedButton(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(Icons.edit),
                              SizedBox(width: 12),
                              Text("Edit " + itemType),
                            ],
                          ),
                        ),
                        onPressed: () async {
                          await context.router
                              .push(EventCreateRoute(event: event));

                          _onRefresh();
                        })
                    : ElevatedButton(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 11.0, horizontal: 8),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                width: 22,
                                height: 22,
                                child: CircularProgressIndicator(
                                    color: Colors.white),
                              ),
                              SizedBox(width: 8),
                              Text("Loading " + itemType)
                            ],
                          ),
                        ),
                        onPressed: () {
                          _onRefresh();
                        }),
              ),
              if (event.isBookable)
                CustomElevatedButton(
                    message: "Voir les réseravtions",
                    icon: Icons.list,
                    onPressed: () async {
                      List<Booking> b = await event.bookings;
                      await context.navigateTo(EventBookingsRoute(bookings: b));
                      _onRefresh();
                    }),
            ],
          ),
        ),
      ],
    );
  }

  void _onRefresh() {
    if (onRefresh != null) onRefresh!();
  }
}
