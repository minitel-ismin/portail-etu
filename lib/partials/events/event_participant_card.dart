import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class EventParticipantsCard extends StatelessWidget {
  const EventParticipantsCard({
    Key? key,
    required this.event,
  }) : super(key: key);

  final PortailEvent event;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.person, size: 22),
                SizedBox(width: 8),
                Text(event.bookingResponses.length.toString() + " participants",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  for (var resp in event.bookingResponses)
                    EventParticipantsUserDetail(event: resp.e!),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class EventParticipantsUserDetail extends StatelessWidget {
  const EventParticipantsUserDetail({
    Key? key,
    required this.event,
  }) : super(key: key);

  final Event event;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: MatrixImageAvatar(
        client: Matrix.of(context).client,
        url: null,
        width: 40,
        height: 40,
        defaultText: event.sender.displayName ?? event.sender.id,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
