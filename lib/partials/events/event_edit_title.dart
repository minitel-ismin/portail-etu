import 'package:another_flushbar/flushbar.dart';
import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/json/event/portail_event_description.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:emse_bde_app/partials/controls/custom_text_field.dart';
import 'package:emse_bde_app/partials/helpers/date_selector.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

class EventEditTitle extends StatefulWidget {
  final BasePortailEvent event;
  final VoidCallback? onSave;
  const EventEditTitle({
    Key? key,
    this.onSave,
    required this.event,
  }) : super(key: key);

  @override
  _EventEditTitleState createState() => _EventEditTitleState();
}

class _EventEditTitleState extends State<EventEditTitle> {
  late PortailEventDescription description;
  bool descEdited = false;
  TextEditingController placeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    description = widget.event.eventDescription;
  }

  Future<void> save() async {
    if (descEdited) {
      if (description.getErrorMessage() == null) {
        await widget.event.setEvenDescription(description);
        descEdited = false;

        setState(() {});
        widget.onSave?.call();
      } else {
        await Flushbar(
          title: "L'évènement comporte une erreur",
          message: description.getErrorMessage(),
          duration: Duration(seconds: 6),
          icon: Icon(Icons.error, color: Colors.white),
          backgroundColor: Colors.red,
          leftBarIndicatorColor: Colors.red[800],
        ).show(context);
      }
    }
  }

  bool settingJoinRules = false;

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(
        constraints: BoxConstraints(maxWidth: 935),
        padding: const EdgeInsets.all(35),
        child: Column(
          children: [
            CustomTextField(
                key: Key("place"),
                name: "Lieu",
                text: description.location,
                onChanged: (newValue) async {
                  description.location = newValue;

                  setState(() {
                    descEdited = true;
                  });
                }),
            if (widget.event.r.canSendDefaultStates)
              SwitchListTile(
                  title: Text("Évènement public "),
                  secondary:
                      settingJoinRules ? CircularProgressIndicator() : null,
                  subtitle: Text(
                      "Permettre de voir l'évènement sans être connecté ?"),
                  value: widget.event.publicEvent,
                  onChanged: !settingJoinRules
                      ? (value) async {
                          setState(() {
                            settingJoinRules = true;
                          });
                          await widget.event.r.setJoinRules(
                              value ? JoinRules.public : JoinRules.private);
                          setState(() {
                            settingJoinRules = false;
                          });
                        }
                      : null),
            DateSelector(
                date: description.timeStart,
                name: "Date du début de l'event",
                onNewDate: (date) async {
                  description.timeStart = date;
                  setState(() {
                    descEdited = true;
                  });
                }),
            DateSelector(
                date: description.timeEnd,
                name: "Date de fin de l'event",
                onNewDate: (date) async {
                  description.timeEnd = date;
                  setState(() {
                    descEdited = true;
                  });
                }),
            if (descEdited)
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: CustomElevatedButton(
                  icon: Icons.edit,
                  message: "Sauvegarder",
                  onFuturePressed: () => save(),
                ),
              )
          ],
        ),
      ),
    ]);
  }
}
