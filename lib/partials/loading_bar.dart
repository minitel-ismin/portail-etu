import 'package:flutter/material.dart';

class LoadingBar extends StatelessWidget {
  const LoadingBar(this.text, {Key? key}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
          SizedBox(
            width: 10,
          ),
          Text(text)
        ],
      ),
    );
  }
}
