import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:flutter/material.dart';

class CustomStatefulView extends StatefulWidget {
  const CustomStatefulView(
      {Key? key,
      required this.header,
      required this.children,
      this.pullToRefresh})
      : super(key: key);

  final CustomHeader header;
  final List<Widget> children;
  final Future<void> Function()? pullToRefresh;

  @override
  _CustomStatefulViewState createState() => _CustomStatefulViewState();
}

class _CustomStatefulViewState extends State<CustomStatefulView> {
  Widget buildListView() {
    return ListView(
      children: [
        widget.header,
        for (Widget child in widget.children) child,
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.pullToRefresh != null
        ? RefreshIndicator(
            onRefresh: widget.pullToRefresh!, child: buildListView())
        : buildListView();
  }
}
