import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:flutter/material.dart';

class CustomList extends StatelessWidget {
  const CustomList(
      {Key? key,
      required this.title,
      required this.children,
      this.actions,
      this.refresh,
      this.hasData = true})
      : super(key: key);

  final VoidCallback? refresh;
  final bool hasData;
  final String title;
  final List<Widget>? children;
  final List<IconButton>? actions;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomHeader(
          title,
          actionButton: actions,
        ),
        if (!hasData) Center(child: CircularProgressIndicator()),
        if (hasData && children != null)
          Expanded(
            child: ListView(children: [for (Widget i in children!) i]),
          ),
        if (hasData && children == null) NoData(),
      ],
    );
  }
}
