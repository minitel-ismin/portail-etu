import 'package:flutter/material.dart';

class CustomSwitchListTile extends StatelessWidget {
  const CustomSwitchListTile(
      {Key? key, this.onChanged, this.title, this.value = true, this.color})
      : super(key: key);
  final Function? onChanged;
  final bool? value;
  final String? title;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      title: Text(title!, style: TextStyle(color: color)),
      value: value!,
      selectedTileColor: color,
      activeColor: color,
      onChanged: onChanged as void Function(bool)?,
    );
  }
}
