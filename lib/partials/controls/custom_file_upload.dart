import 'package:emse_bde_app/partials/loading_bar.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:nanoid/nanoid.dart';

class CustomImageUploader extends StatefulWidget {
  const CustomImageUploader({Key? key, required this.room, this.onUpdate})
      : super(key: key);

  final Room room;
  final Function()? onUpdate;

  @override
  _CustomImageUploaderState createState() => _CustomImageUploaderState();
}

class _CustomImageUploaderState extends State<CustomImageUploader> {
  bool uploadingFile = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: uploadingFile
          ? LoadingBar("Uploading ...")
          : TextButton(
              onPressed: () async {
                FilePickerResult? result = await FilePicker.platform
                    .pickFiles(type: FileType.image, withData: true);
                if (result?.files.isNotEmpty == true &&
                    result?.files.first.bytes == null) return;
                setState(() {
                  uploadingFile = true;
                });
                String filename = "event_img_" +
                    nanoid() +
                    (result!.files.first.extension ?? '');

                MatrixFile mFile = MatrixFile(
                    bytes: result!.files.first.bytes!, name: filename);
                await widget.room.setAvatar(mFile);
                setState(() {
                  uploadingFile = false;
                });
                widget.onUpdate!();
              },
              child: Column(
                children: [
                  MatrixImageAvatar(
                      client: widget.room.client,
                      url: widget.room.avatar,
                      width: 180,
                      height: 180,
                      defaultIcon: Icon(Icons.image,
                          color: Theme.of(context).colorScheme.onBackground,
                          size: 45)),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Set image",
                        style: Theme.of(context).textTheme.bodyText1),
                  )
                ],
              ),
            ),
    );
  }
}
