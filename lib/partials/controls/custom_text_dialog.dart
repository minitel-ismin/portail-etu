import 'package:emse_bde_app/partials/controls/custom_text_field.dart';
import 'package:flutter/material.dart';

class CustomTextDialog extends StatefulWidget {
  final String name;
  final String initialText;
  final int? maxLines;
  const CustomTextDialog(
      {Key? key,
      required this.initialText,
      required this.name,
      this.maxLines = 1})
      : super(key: key);

  @override
  _CustomTextDialogState createState() => _CustomTextDialogState();
}

class _CustomTextDialogState extends State<CustomTextDialog> {
  String? text;
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(widget.name),
      children: [
        CustomTextField(
            name: widget.name,
            text: widget.initialText,
            maxLines: widget.maxLines,
            onChanged: (newValue) async {
              text = newValue;
              setState(() {});
            }),
        Row(
          children: [
            TextButton(
              // FlatButton widget is used to make a text to work like a button

              onPressed: () {
                Navigator.of(context).pop();
              }, // function used to perform after pressing the button
              child: Text('CANCEL'),
            ),
            TextButton(
              onPressed: text == null
                  ? null
                  : () {
                      Navigator.of(context).pop(text);
                    },
              child: Text('ACCEPT'),
            ),
          ],
        ),
      ],
    );
  }
}
