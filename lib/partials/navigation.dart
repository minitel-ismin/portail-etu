import 'package:auto_route/auto_route.dart';
import 'package:emse_bde_app/router.gr.dart';
import 'package:flutter/material.dart';

class NavBarDesktop extends StatelessWidget {
  const NavBarDesktop({Key? key}) : super(key: key);

  Widget buildMyAccountButton(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.person),
        onPressed: () {
          context.router.navigate(UserRoute());
        });
  }

  Widget buildNavigationButton(BuildContext context, String text, IconData icon,
      {required PageRouteInfo<dynamic> route}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(6),
              child: Icon(icon),
            ),
            Text(text),
          ],
        ),
        onPressed: () {
          context.navigateTo(route);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).cardColor.withOpacity(0.8),
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Row(
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Image(
                        image: AssetImage('assets/images/piaf.jpg'),
                        width: 40,
                        height: 40,
                      )),
                  SizedBox(width: 8),
                  Text("Le PIAF",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.w600))
                ],
              ),
            ),
            onPressed: () => context.navigateTo(HomeRoute()),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildNavigationButton(context, "Assos", Icons.people,
                    route: AssoWrapperRoute()),
                buildNavigationButton(context, "Events", Icons.event,
                    route: EventsRoute()),
                buildNavigationButton(context, "Store", Icons.store,
                    route: StoreRoute()),
                buildNavigationButton(context, "Location", Icons.key,
                    route: RentalsRoute()),
                buildNavigationButton(context, "Chat", Icons.chat,
                    route: RoomListWrapperRoute()),
              ],
            ),
          ),
          buildMyAccountButton(context)
        ],
      ),
    );
  }
}

class NavBarMobile extends StatefulWidget {
  final TabsRouter tabsRouter;
  const NavBarMobile({Key? key, required this.tabsRouter}) : super(key: key);

  @override
  _NavBarMobileState createState() => _NavBarMobileState();
}

class _NavBarMobileState extends State<NavBarMobile> {
  @override
  Widget build(BuildContext context) {
    TabsRouter tabsRouter = widget.tabsRouter;

    List<BottomNavigationBarItem> items = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Home',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.event),
        label: 'Events',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.people),
        label: 'Assos',
      ),
      BottomNavigationBarItem(icon: Icon(Icons.person), label: 'User'),
      BottomNavigationBarItem(icon: Icon(Icons.chat), label: 'Chat')
    ];

    return BottomNavigationBar(
        currentIndex: tabsRouter.activeIndex < items.length - 1
            ? tabsRouter.activeIndex
            : 0,
        onTap: (int pos) => pos < items.length
            ? tabsRouter.setActiveIndex(pos)
            : context.navigateTo(RoomListWrapperRoute()),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedItemColor: Theme.of(context).colorScheme.onSurface,
        unselectedItemColor: Theme.of(context).colorScheme.onSurface,
        unselectedIconTheme: IconThemeData(
            color: Theme.of(context).colorScheme.onSurface, size: 24),
        selectedIconTheme: IconThemeData(
            color: Theme.of(context).colorScheme.onSurface, size: 26),
        items: items);
  }
}
