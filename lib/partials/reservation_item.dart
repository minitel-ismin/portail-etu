import 'package:emse_bde_app/logic/models/event/base_portail_event.dart';
import 'package:emse_bde_app/logic/models/event/portail_event.dart';
import 'package:flutter/material.dart';

class ReservationItem extends StatelessWidget {
  ReservationItem(this.event);

  final PortailEvent? event;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: MaterialButton(
        shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(12)),
        onPressed: () {},
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        event!.type == PortailEventTypes.reservation
                            ? "Location " + event!.name
                            : event!.name,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.normal)),
                    if (event!.description != "")
                      Padding(
                        padding: const EdgeInsets.all(4),
                        child: Text(
                          event!.description,
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.normal),
                          overflow: TextOverflow.clip,
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
