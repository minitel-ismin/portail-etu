import 'package:emse_bde_app/logic/models/json/event/booking_question.dart';
import 'package:emse_bde_app/logic/models/json/event/booking_response.dart';
import 'package:flutter/material.dart';

class FormInputPartial extends StatefulWidget {
  const FormInputPartial(this.question, this.result, {this.onRefresh});
  final BookingQuestion question;
  final BookingResponse result;
  final Function? onRefresh;

  @override
  _FormInputPartialState createState() => _FormInputPartialState();
}

class _FormInputPartialState extends State<FormInputPartial> {
  TextEditingController controller = TextEditingController();

  void refresh() {
    if (widget.onRefresh != null) widget.onRefresh!();
  }

  @override
  void initState() {
    super.initState();
    String text = "";

    if (widget.question.type == QuestionType.text &&
        widget.result.answers.isNotEmpty) {
      text = widget.result.answers.first;
    }
    controller = TextEditingController(text: text);
  }

  @override
  Widget build(BuildContext context) {
    BookingQuestion question = widget.question;
    BookingResponse response = widget.result;

    if (question.type == QuestionType.title) {
      return Text(question.question?.title ?? "",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold));
    } else {
      List<Widget> children = [
        if (question.question?.title != null) Text(question.question!.title)
      ];

      if (question.type == QuestionType.text) {
        children.add(TextField(
          controller: controller,
          onChanged: (text) {
            response.answers = [text];
          },
        ));
      } else if (question.type == QuestionType.choice) {
        if (question.max_selection != 1) {
          for (BookingQuestionAnswers o in question.answers) {
            children.add(CheckboxListTile(
                value: response.answers.contains(o.id),
                title: Text(o.text),
                onChanged: (bool? val) {
                  setState(() {
                    if (val == true) {
                      response.answers.add(o.id);
                    } else {
                      response.answers.remove(o.id);
                    }
                  });
                  refresh();
                }));
          }
        } else {
          for (BookingQuestionAnswers o in question.answers) {
            children.add(
              RadioListTile(
                  title: Text(o.id.toString()),
                  subtitle: Text((o.cost != 0 && o.cost != null)
                      ? o.cost.toString() + " euros"
                      : "Gratuit"),
                  value: o.id,
                  groupValue: response.answers[0],
                  toggleable: question.optional,
                  onChanged: (dynamic value) {
                    setState(() {
                      response.answers = [value.toString()];
                    });
                    refresh();
                  }),
            );
          }
        }
      } else {
        children.add(Icon(Icons.error));
      }

      return Column(
          children: children, crossAxisAlignment: CrossAxisAlignment.start);
    }
  }
}
