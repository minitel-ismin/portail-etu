import 'package:flutter/material.dart';

class PreviewIndicator extends StatelessWidget {
  const PreviewIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
          color: Colors.red,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
            child: Row(
              children: [
                Icon(Icons.warning, color: Colors.white, size: 30),
                SizedBox(width: 15),
                Expanded(
                  child: Text("Prévisualisation de l'évènement",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w600)),
                ),
              ],
            ),
          )),
    );
  }
}
