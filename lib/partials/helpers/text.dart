import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class H1Title extends StatelessWidget {
  const H1Title(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Text(text,
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600)),
    );
  }
}

class H2Title extends StatelessWidget {
  final AsyncCallback? onPressed;
  final String text;

  const H2Title(this.text, {this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4),
      child: Card(
        color: Theme.of(context).primaryColor,
        child: MaterialButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.all(8),
          onPressed: onPressed,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(text,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Theme.of(context).colorScheme.onPrimary)),
          ),
        ),
      ),
    );
  }
}
