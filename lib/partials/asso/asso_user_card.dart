import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:emse_bde_app/partials/no_data.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart' as matrix;
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/view/room_page.dart';

class AssoUserCard extends StatefulWidget {
  final Position position;
  const AssoUserCard({Key? key, required this.position}) : super(key: key);

  @override
  _AssoUserCardState createState() => _AssoUserCardState();
}

class _AssoUserCardState extends State<AssoUserCard> {
  final Radius radius = Radius.circular(6);

  @override
  Widget build(BuildContext context) {
    final mClient = Matrix.of(context).client;
    return MaterialButton(
      onPressed: () async {
        String roomId =
            await mClient.startDirectChat(widget.position.user!.mxId!);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Scaffold(
                    appBar: AppBar(title: Text("Event chat")),
                    body: RoomPage(
                        client: Matrix.of(context).client, roomId: roomId))));
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      padding: EdgeInsets.all(10),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(radius)),
        child: widget.position.user?.mxId == null
            ? NoData()
            : FutureBuilder<matrix.ProfileInformation>(
                future: mClient.getUserProfile(widget.position.user!.mxId!),
                builder: (context, snap) {
                  return Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: MatrixImageAvatar(
                          client: Matrix.of(context).client,
                          shape: MatrixImageAvatarShape.none,
                          height: 60,
                          width: 60,
                          borderRadius: BorderRadius.only(
                              topLeft: radius, bottomLeft: radius),
                          defaultText: snap.data?.displayname ??
                              widget.position.user!.displayName,
                          backgroundColor: Colors.blue,
                          url: snap.data?.avatarUrl,
                          fit: true,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              maxWidth: 140,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(widget.position.role?.name ?? "role",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600)),
                                      SizedBox(height: 4),
                                      Text(snap.data?.displayname ??
                                          widget.position.user!.displayName),
                                      if (widget.position.user?.type !=
                                          "matrix")
                                        Text(
                                          widget.position.user!.type! +
                                              " " +
                                              widget.position.user!.promo
                                                  .toString(),
                                          style: TextStyle(fontSize: 13),
                                        ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                }),
      ),
    );
  }
}
