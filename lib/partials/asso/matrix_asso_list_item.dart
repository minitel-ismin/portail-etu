import 'package:cached_network_image/cached_network_image.dart';

import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

class MatrixAssoListItem extends StatelessWidget {
  final Room assos;
  final Function()? onPressed;
  const MatrixAssoListItem({Key? key, this.onPressed, required this.assos})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final m = Matrix.of(context).client;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 6),
      child: MaterialButton(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: Row(
                children: [
                  Container(
                    width: 52,
                    height: 52,
                    decoration: BoxDecoration(
                        color: Theme.of(context).cardColor,
                        borderRadius: BorderRadius.circular(8),
                        image: assos.avatar != null
                            ? DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                  assos.avatar!
                                      .getThumbnail(m, width: 50, height: 50)
                                      .toString(),
                                ))
                            : null),
                    child: assos.avatar != null
                        ? null
                        : Icon(Icons.device_unknown, size: 30),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(assos.displayname, style: TextStyle(fontSize: 20)),
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0),
                          child: Text(assos.topic,
                              maxLines: 2, overflow: TextOverflow.ellipsis),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
              Icon(Icons.navigate_next, size: 20),
            ],
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
