import 'package:emse_bde_app/logic/models/association_data.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/dialogs/custom_dialogs.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class AssoPortailCard extends StatelessWidget {
  final Room room;
  const AssoPortailCard({Key? key, required this.room}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MatrixImageAvatar(
          width: 52,
          height: 52,
          url: room.avatar,
          client: room.client,
          defaultText: room.name,
          backgroundColor: Colors.blue,
        ),
        SizedBox(
          width: 16,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(room.name, style: TextStyle(fontSize: 20)),
              if (room.topic != "")
                Padding(
                  padding: const EdgeInsets.only(top: 2.0),
                  child: Text(room.topic,
                      maxLines: 2, overflow: TextOverflow.ellipsis),
                ),
              Text(room.summary.mJoinedMemberCount.toString() +
                  (room.summary.mJoinedMemberCount == 1
                      ? " membre"
                      : " membres")),
              if (room
                  .canSendDefaultStates) // TODO : see if the user can send state event
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: MaterialButton(
                      child: Text("Create asso"),
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        String? name = await CustomDialogs.showCustomTextDialog(
                          context,
                          title: "Creer une nouvelle association",
                          helpText: "Nom de l'association",
                          initialText: "",
                        );
                        if (name != null) {
                          String? topic =
                              await CustomDialogs.showCustomTextDialog(
                            context,
                            title: "Choisir la description",
                            helpText: "Tag de l'association",
                          );
                          if (topic != null)
                            await AssociationData.createAsso(
                                Matrix.of(context).client,
                                name: name,
                                topic: topic,
                                parentSpace: room);
                        }
                      }),
                )
            ],
          ),
        ),
      ],
    );
  }
}
