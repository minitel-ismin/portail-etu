import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:emse_bde_app/partials/controls/custom_elevetated_button.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix/src/utils/space_child.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';

class AssoPortailChild extends StatelessWidget {
  final SpaceRoomsChunk space;
  final SpaceChild? spaceChild;
  final VoidCallback onJoin;

  const AssoPortailChild(
      {Key? key,
      required this.space,
      required this.spaceChild,
      required this.onJoin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final c = Matrix.of(context).client;
    return Row(
      children: [
        MatrixImageAvatar(
          width: 52,
          height: 52,
          url: null,
          client: c,
          defaultText: space.name,
          backgroundColor: Colors.blue,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(space.name ?? space.roomId,
                    style: TextStyle(fontSize: 18)),
                SizedBox(height: 2),
                if (space.topic != null) Text(space.topic!, maxLines: 3),
                Text(space.numJoinedMembers.toString() +
                    (space.numJoinedMembers == 1 ? " membre" : " membres")),
                if (spaceChild?.suggested == true) Text("Suggested room")
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(4),
          child: CustomElevatedButton(
              message: "Join",
              icon: Icons.settings_input_hdmi_outlined,
              onFuturePressed: () async {
                await c.joinRoom(space.roomId, serverName: spaceChild?.via);
                onJoin();
              }),
        )
      ],
    );
  }
}
