import 'package:flutter/material.dart';

class IconText extends StatelessWidget {
  const IconText({Key? key, this.text, this.icon, this.title, this.subtitle})
      : super(key: key);
  final String? text;
  final IconData? icon;
  final String? title;
  final Column? subtitle;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        children: [
          Icon(icon),
          SizedBox(width: 8),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (title != null)
                  Text(title!, style: TextStyle(fontWeight: FontWeight.w600)),
                if (text != null) Text(text!),
                if (subtitle != null) subtitle!
              ],
            ),
          ),
        ],
      ),
    );
  }
}
