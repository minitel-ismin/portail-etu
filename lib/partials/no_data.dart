import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  final String? message;
  NoData({Key? key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.no_drinks),
          SizedBox(
            width: 12,
          ),
          Flexible(child: Text(message ?? "Failed to get data")),
        ],
      ),
    );
  }
}
