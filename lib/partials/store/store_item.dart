import 'package:cached_network_image/cached_network_image.dart';

import 'package:emse_bde_app/logic/models/event/store_item.dart';
import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';

class StoreItemWidget extends StatefulWidget {
  final StoreItem item;
  const StoreItemWidget(this.item, {Key? key}) : super(key: key);

  @override
  _StoreItemWidgetState createState() => _StoreItemWidgetState();
}

class _StoreItemWidgetState extends State<StoreItemWidget> {
  @override
  Widget build(BuildContext context) {
    StoreItem item = widget.item;
    final m = Matrix.of(context).client;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            decoration: BoxDecoration(
                color: Colors.grey[400],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12)),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: item.r.avatar != null
                      ? CachedNetworkImageProvider(item.r.avatar!
                          .getThumbnail(m, width: 80, height: 80)
                          .toString()) as ImageProvider
                      : AssetImage('assets/images/event_placeholder.jpg'),
                )),
            height: 120,
            width: 200,
            child: item.r.avatar != null
                ? null
                : Icon(Icons.image, size: 64, color: Colors.white)),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text(
                          item.name,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Text(item.eventBooking.displayCost,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w600)),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
