import 'package:cached_network_image/cached_network_image.dart';
import 'package:emse_bde_app/partials/custom_view/custom_header.dart';
import 'package:matrix/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class MImage extends StatelessWidget {
  const MImage({Key? key, required this.event}) : super(key: key);
  final Event event;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) {
            return SafeArea(
              child: Scaffold(
                  body: Column(
                children: [
                  CustomHeader("Image dispaly " + event.body),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: InteractiveViewer(
                          boundaryMargin: EdgeInsets.all(20.0),
                          minScale: 0.01,
                          maxScale: 4,
                          child: MImageDisplay(event: event)),
                    ),
                  ),
                ],
              )),
            );
          }));
        },
        child: MImageDisplay(event: event));
  }
}

class MImageDisplay extends StatelessWidget {
  const MImageDisplay({Key? key, required this.event}) : super(key: key);
  final Event event;

  Widget getImage(url) {
    return Material(
      elevation: 3,
      borderRadius: BorderRadius.all(Radius.circular(5)),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: url != null
              ? CachedNetworkImage(
                  imageUrl: url,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      CircularProgressIndicator(
                          value: downloadProgress.progress),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
              : Icon(Icons.error)),
    );
  }

  @override
  Widget build(BuildContext context) {
    String url = event.getAttachmentUrl(getThumbnail: true).toString();
    int? wi = event.infoMap["w"];
    int? hi = event.infoMap["h"];
    double ratio = 1;

    if (hi != null && wi != null) {
      ratio = wi / hi;
    }

    int cache_size = 600;
    //int cache_h = cache_size ~/ ratio;
    int cache_w = (cache_size * ratio).toInt();

    if (event.isAttachmentEncrypted) {
      return AspectRatio(
          aspectRatio: ratio,
          child: FutureBuilder<MatrixFile>(
            future: event.downloadAndDecryptAttachment(
              downloadCallback: (Uri url) async {
                final file =
                    await DefaultCacheManager().getSingleFile(url.toString());
                return await file.readAsBytes();
              },
            ),
            builder: (BuildContext context, AsyncSnapshot<MatrixFile> file) {
              if (file.hasData) {
                return ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.memory(
                      file.data!.bytes,
                      fit: BoxFit.fill,
                      //cacheHeight: cache_h,
                      cacheWidth: cache_w,
                    ));
              }
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(30),
                  child: CircularProgressIndicator(),
                ),
              );
            },
          ));
    }
    return getImage(url);
  }
}
