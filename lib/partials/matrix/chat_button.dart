import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/partials/dialogs/adaptative_dialogs.dart';
import 'package:minestrix_chat/partials/matrix/matrix_image_avatar.dart';
import 'package:minestrix_chat/utils/matrix_widget.dart';
import 'package:minestrix_chat/view/room_page.dart';
import 'package:timeago/timeago.dart' as timeago;

class ChatButton extends StatelessWidget {
  const ChatButton({Key? key, required this.r, this.enableName = false})
      : super(key: key);

  final Room r;
  final bool enableName;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      var e = r.lastEvent!;
      return MaterialButton(
        onPressed: () {
          AdaptativeDialogs.show(
              context: context,
              builder: (context) => RoomPage(
                  client: Matrix.of(context).client,
                  roomId: r.id,
                  onBack: () {
                    Navigator.of(context).pop();
                  }));
        },
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            MatrixImageAvatar(
                url: e.sender.avatarUrl,
                width: 38,
                height: 38,
                defaultText: e.sender.displayName,
                client: r.client),
            SizedBox(
              width: 8,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (enableName)
                  Text(r.displayname,
                      maxLines: 3,
                      style: TextStyle(
                          fontSize: 14,
                          color: Theme.of(context).colorScheme.onPrimary)),
                Text(e.body,
                    maxLines: 3,
                    style: TextStyle(
                        fontSize: 14,
                        color: Theme.of(context).colorScheme.onPrimary)),
                Text(timeago.format(e.originServerTs),
                    style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w300,
                        color: Theme.of(context).colorScheme.onPrimary))
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Icon(Icons.message,
                  color: Theme.of(context).colorScheme.onPrimary),
            )
          ],
        ),
      );
    });
  }
}
