# Event proposal (aka Event as room)

This proposal is to describe how we are going to use matrix room to store event details.

**//TODO** : Find another name than event. It's too confusing with the existing matrix terminology.

```
fr.emse.minitel.portail.event
```

We may want to wait for the social media on matrix MSC to land.

When we create an event, the user creating the event should invite all the members of the parent space.
If the user wants to decline the event, all he has to do is to leave the room.

The client should propose the event create to bulk invite users with like a button. Bulk invite should not be made 

## Event description [state]

Inherited properties from the room

* name -> `m.room.name`
* avatar -> `m.room.avatar`
* about -> `m.room.topic`

However we have created a custom state to store more information about the event.

```json
{
    "key":"",
    "content":{
        "location": "the location of the event",
        "time_start" : <timestamp>,
        "time_end" : <timestamp>
    }
}
```

## Event booking

Here we are going to describe how participants can book.

Indeed, when the user has been invited or did joined the room he is considered as invited. We need to define a way for the user to say that he is comming. We could use polls to do this [MSC3381 - Polls (mk II)](https://github.com/matrix-org/matrix-doc/pull/3381). However, we wanted to allow the event organisator to do a more complex form.

Despite this, we tries to be as close as possible to the poll specification.

Example:
```json
{
    "type": "fr.emse.minitel.booking",
    "key": "",
    "sender": "@bob:example.com",
    "content": {
        "cost": 0,
        "form_questions": [
            {
                "id": "1",
                "type": "title",
                "question": {
                    "m.text": "What is your name ?"
                }
            },
            {
                "id": "2",
                "type": "choice",
                "question": {
                    "m.text": "How are you comming ?"
                },
                "max_selection": 1,
                "answers": [
                    {"id": "car", "m.text": "Car",},
                    {"id": "horse", "m.text": "Horse",},
                    {"id": "unicorn", "m.text": "Going to ride my unicorn"}
                ]
            },
            {
                "id": "3",
                "type": "text",
                "question": {
                    "m.text": "What is your name ?"
                }
            }
            {
                "id": "4",
                "type": "choice",
                "question": {
                    "m.text": "Do you want a drink ?"
                },
                
                "optional": true,
                "answers": [
                    { "id": "water", "m.text": "Some water"},
                    { "id": "red_wine", "m.text": "A nice bottle of red wine", "cost": 800}, // 8.00 euros
                ]
            }
        ],
        "currency": "eur"
    }
}
```
| Name       | Type     | Description                                                                                                                                                                                                                                                                                                        | Default                                                       |
| ---------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------- |
| `id`       | required | the id of the question.                                                                                                                                                                                                                                                                                            |
| `type`     | optional | The type of the question. Could be the following types: <li>`title` the question is just a title. No answer possible.</li>                                                          <li>`text` the response type is a string.</li> <li>`choice` the response type is a subset of the possibles answers given.</li> | choice                                                        |
| `cost`     | optional | the cost of this event or for this specific answer. The value should be given as an integer. For example, for euro, we should give the price in cents.                                                                                                                                                             | 0                                                             |
| `currency` | optional | the currency used to display the cost `ISO 4217 `.                                                                                                                                                                                                                                                                 | `eur`.                                                        |
| `optional` | optional | describe if the user must respond to this question or not.                                                                                                                                                                                                                                                         | `false`. *An answer to this question is required by default.* |

### The event form [state]


```json
{
    "type": "fr.emse.minitel.booking.response",
    "key": <userid>,
    "content":{
        "response": "going/declined",
        "form_responses": [
            { "id": 2, "answers": ["unicorn"]},
            { "id": 3, "answers": ["Bob"]},
            { "id": 4, "answers": ["water", "red_wine"]}
        ],
        "m.relates_to": {
            "rel_types": "m.reference",
            "event_id": "$form"
        }
    }
}

```

* `response` Response to this event. Could be `going` or `declined`.
* `form_response` Response to the form described. Optional.

With the following response, the client should be able to calculate the total cost of this reservation.

**NB**: Power levels should be edited to let the user respond to the booking.

### The booking response [state]


## Posts [event] (aka News event)

Post event is described in [MSC3639 - Matrix for the social media use case](https://github.com/matrix-org/matrix-doc/pull/3639)

## Random thoughts

**Idea**

We may want to think of an event as a space. By this it will allow us to have linked room like

* space : store main details about the event

* Organization room : to store events like event organization calendar, discussions between team organization.


**Idea** : Event Restriction

We should add a way to restrict the event registration to people in the related association. (Évènement réservé aux cotisants).