# The booking madness

We need to make the distinction between

* `portailEvent` Is here to store a one recurrence event. The goal being to get all the bookings easily. User are supposed to be able to book only one time. Bookings are stored as states.
* `storeItem` A class to represent a store item or item to be rented. It's supposed to be a long living room. User are supposed to be able to book multiple time for different times (e.g. for different times). Bookings are stored as events.

```mermaid
classDiagram

    class Room {
        + String name
        + Uri avatar
        + List~Events~ events
    }

    class BasePortailEvent{
        + Room r
        + PortailEventDescription eventDescription
        + PortailEventBooking eventBooking
        
    }

    class PortailEvent
    class StoreItem
    class RentalItem

     BasePortailEvent <|-- BasePortailItem
    
    BasePortailItem <|-- StoreItem
    BasePortailItem <|-- RentalItem 
    BasePortailEvent <|-- PortailEvent 

    BasePortailItem *.. PortailEventBookingResponse : Relation as state
    PortailEvent *.. PortailEventBookingResponse : Relation as event



    BasePortailEvent *.. PortailEventBooking : from state
    BasePortailEvent *.. PortailEventDescription : from state
    BasePortailEvent *.. BaseModel : store
    BaseModel *.. Room
    
    class PortailEventBooking{
        +isBookable
        +form_questions(List~BookingQuestion~)
        +currency
        +base_cost
    }
    
    class BookingQuestion{
        +id(String)
        +type(QuestionType)
        +max_selection(int)
        +question(BookingQuestionTitle)
        +optional(bool)
        +answers(List~BookingQuestionAnswers~)
    }

    class BookingQuestionTitle {
        +id(String)
    }

    class BookingQuestionAnswers {
        +id(String)
        +text(String)
        +cost(int)
    }

    PortailEventBooking *.. BookingQuestion
    BookingQuestion *.. BookingQuestionTitle
    BookingQuestion *.. BookingQuestionAnswers

    class PortailEventBookingResponse {
        +Event relates_to
        +Map~String, BookingResponse~ responses
        +ResponseType type
    }

    class Event
    class BookingResponse{
        +String id
        +List~String~ answers
    }

    PortailEventBookingResponse *.. Event : store
    PortailEventBookingResponse *.. BookingResponse

    class PortailEventDescription{
        +String? location
        +DateTime? time_start
        +DateTime? time_end
        getErrorMessage()
    }

    class Booking {
        isChecked() bool
        getUser() User
        getCreatedAt() DateTime
        getCost() int
    }

    Booking .. PortailEventBooking
    Booking .. PortailEventBookingResponse

```